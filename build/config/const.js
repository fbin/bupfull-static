"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CRON_NAME_DIC = exports.QL_PREFIX = exports.TASK_PREFIX = exports.QL_COMMAND = exports.TASK_COMMAND = exports.LOG_END_SYMBOL = void 0;
exports.LOG_END_SYMBOL = '　　　　　';
exports.TASK_COMMAND = 'task';
exports.QL_COMMAND = 'ql';
exports.TASK_PREFIX = `${exports.TASK_COMMAND} `;
exports.QL_PREFIX = `${exports.QL_COMMAND} `;
exports.CRON_NAME_DIC = {
    '1': 'bili点赞',
    '2': 'bili投币',
    '3': 'bili收藏',
    '4': 'bili一键三连',
    '5': 'bili分享',
    '6': 'bili每日任务',
    '7': 'bili观看视频',
    '8': 'bili发布评论',
    '9': 'bili删除评论'
};
//# sourceMappingURL=const.js.map