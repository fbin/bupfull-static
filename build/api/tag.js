"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const typedi_1 = require("typedi");
const express_rate_limit_1 = __importDefault(require("express-rate-limit"));
const tag_1 = __importDefault(require("../services/tag"));
const celebrate_1 = require("celebrate");
const multer_1 = __importDefault(require("multer"));
const config_1 = __importDefault(require("../config"));
const route = (0, express_1.Router)();
const storage = multer_1.default.diskStorage({
    destination: function (req, file, cb) {
        cb(null, config_1.default.scriptPath);
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    },
});
const upload = (0, multer_1.default)({ storage: storage });
exports.default = (app) => {
    app.use('/tags', route);
    route.get('/', async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const tagService = typedi_1.Container.get(tag_1.default);
            const data = await tagService.tags(req.query.searchValue);
            return res.send({ code: 200, data });
        }
        catch (e) {
            logger.error('🔥 error: %o', e);
            return next(e);
        }
    });
    route.get('/search', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
            type: celebrate_1.Joi.string().required(),
            search_text: celebrate_1.Joi.string(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const tagService = typedi_1.Container.get(tag_1.default);
            const data = await tagService.search({
                type: req.query.type,
                search_text: req.query.search_text,
            });
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.array().items(celebrate_1.Joi.object({
            id: celebrate_1.Joi.string().required(),
            type: celebrate_1.Joi.string().required(),
            name: celebrate_1.Joi.string().required(),
        })),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const tagService = typedi_1.Container.get(tag_1.default);
            const data = await tagService.create(req.body);
            return res.send({ code: 200, data });
        }
        catch (e) {
            return next(e);
        }
    });
    route.put('/', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            type: celebrate_1.Joi.string().required(),
            name: celebrate_1.Joi.string().required(),
            id: celebrate_1.Joi.string().required(),
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const tagService = typedi_1.Container.get(tag_1.default);
            const data = await tagService.update(req.body);
            return res.send({ code: 200, data });
        }
        catch (e) {
            return next(e);
        }
    });
    route.delete('/', (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.array().items(celebrate_1.Joi.string().required()),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const tagService = typedi_1.Container.get(tag_1.default);
            const data = await tagService.remove(req.body);
            return res.send({ code: 200, data });
        }
        catch (e) {
            return next(e);
        }
    });
};
//# sourceMappingURL=tag.js.map