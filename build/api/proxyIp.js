"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const typedi_1 = require("typedi");
const express_rate_limit_1 = __importDefault(require("express-rate-limit"));
const celebrate_1 = require("celebrate");
const proxyIp_1 = __importDefault(require("../services/proxyIp"));
const util_1 = require("../config/util");
const route = (0, express_1.Router)();
/**
 * 代理IP
 */
exports.default = (app) => {
    app.use('/proxyIp', route);
    route.get('/list', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        query: celebrate_1.Joi.object({
            t: celebrate_1.Joi.any(),
            page_index: celebrate_1.Joi.number().required().min(1).max(99999),
            page_size: celebrate_1.Joi.number().required().min(10).max(100),
            search_text: celebrate_1.Joi.any()
        })
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            // const accessToken = getToken(req);
            const proxyIpService = typedi_1.Container.get(proxyIp_1.default);
            const data = await proxyIpService.getList({
                page_index: Number(req.query.page_index),
                page_size: Number(req.query.page_size),
                search_text: String(req.query.search_text)
            });
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/modify', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            ip: celebrate_1.Joi.string().required(),
            ip_type: celebrate_1.Joi.string().required(),
            city_code: celebrate_1.Joi.string().required(),
            city_name: celebrate_1.Joi.string().required(),
            isp_code: celebrate_1.Joi.string().required(),
            isp_name: celebrate_1.Joi.string().required(),
            effective_time: celebrate_1.Joi.date().required(),
            expire_time: celebrate_1.Joi.date().required()
        }),
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const proxyIpService = typedi_1.Container.get(proxyIp_1.default);
            const data = await proxyIpService.modify(Object.assign({}, req.body), accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/del', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            ip: celebrate_1.Joi.string().required()
        })
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const proxyIpService = typedi_1.Container.get(proxyIp_1.default);
            const data = await proxyIpService.del(Object.assign({}, req.body), accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
    route.post('/changeEnable', (0, express_rate_limit_1.default)({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }), (0, celebrate_1.celebrate)({
        body: celebrate_1.Joi.object({
            ip: celebrate_1.Joi.string().required(),
            enable_flag: celebrate_1.Joi.boolean().required()
        })
    }), async (req, res, next) => {
        const logger = typedi_1.Container.get('logger');
        try {
            const accessToken = (0, util_1.getToken)(req);
            const proxyIpService = typedi_1.Container.get(proxyIp_1.default);
            const data = await proxyIpService.changeEnable(Object.assign({}, req.body), accessToken);
            return res.send(data);
        }
        catch (e) {
            return next(e);
        }
    });
};
//# sourceMappingURL=proxyIp.js.map