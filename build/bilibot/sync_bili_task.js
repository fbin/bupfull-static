"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.syncBiliTask = void 0;
const typedi_1 = require("typedi");
const biliTask_1 = __importDefault(require("../services/biliTask"));
const syncBiliTask = async (call) => {
    const logger = typedi_1.Container.get('logger');
    const biliTaskService = new biliTask_1.default(logger);
    call.on('data', async (request) => {
        // 处理客户端发送的请求消息
        logger.info('[Received request], [RecordId]: %s, [RunTasks]: %s', request.recordId, request.runTasks);
        // 统计任务结果
        if (request.taskResult) {
            if (biliTaskService.COMMENT_RUN_TASKS == request.runTasks ||
                biliTaskService.CONTROL_COMMENT_RUN_TASKS == request.runTasks) {
                // 评论任务
                await biliTaskService.countCommentTaskResult(Object.assign({}, request.taskResult));
            }
            else {
                await biliTaskService.countSimpleTaskResult(Object.assign({}, request.taskResult));
            }
            return;
        }
        // 任务执行结束, 更新任务状态
        if (request.completed) {
            if (biliTaskService.COMMENT_RUN_TASKS == request.runTasks ||
                biliTaskService.CONTROL_COMMENT_RUN_TASKS == request.runTasks) {
                // 评论任务
                await biliTaskService.endCommentTask({
                    recordId: request.recordId,
                });
            }
            else {
                await biliTaskService.endSimpleTask({
                    recordId: request.recordId,
                });
            }
            return;
        }
        // 获取响应并返回数据
        let biliTask;
        if (biliTaskService.COMMENT_RUN_TASKS == request.runTasks ||
            biliTaskService.CONTROL_COMMENT_RUN_TASKS == request.runTasks) {
            // 评论任务
            biliTask = await biliTaskService.getBiliCommentTaskResp({
                record_id: request.recordId,
                run_tasks: request.runTasks,
            });
        }
        else {
            biliTask = await biliTaskService.getBiliSimpleTaskResp({
                record_id: request.recordId,
            });
        }
        const biliTaskResponse = {
            command: '1',
            reply: '2',
            biliTask,
        };
        logger.info('Write [%s] response: %s', request.runTasks, JSON.stringify(biliTaskResponse));
        call.write(biliTaskResponse);
    });
    // 数据流结束
    call.on('end', () => {
        call.end(); // 结束数据流
        logger.info('Server BiliTask 处理完成');
    });
};
exports.syncBiliTask = syncBiliTask;
//# sourceMappingURL=sync_bili_task.js.map