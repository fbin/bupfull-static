"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.syncBiliAccount = void 0;
const biliAccount_1 = __importDefault(require("../services/biliAccount"));
const typedi_1 = require("typedi");
const biliAccount_2 = require("../data/models/biliAccount");
const syncBiliAccount = async (call, callback) => {
    var _a, _b, _c, _d, _e, _f;
    const logger = typedi_1.Container.get('logger');
    const callAccount = call.request.biliAccount;
    if (callAccount != null) {
        const vipType = callAccount.vipType;
        const vipStatus = callAccount.vipStatus;
        logger.debug('昵称: ' + callAccount.uname);
        logger.debug('VipType: ' + vipType);
        logger.debug('VipStatus: ' + vipStatus);
        //转换为BiliAccount
        var biliAccount;
        //const biliAccountService = Container.get(BiliAccountService);
        const biliAccountService = new biliAccount_1.default(logger);
        const doc = await biliAccountService.getDb({ mid: callAccount.mid });
        if (doc != null) {
            biliAccount = new biliAccount_2.BiliAccount({
                mid: callAccount.mid,
                name: callAccount.uname,
                avatar: callAccount.face,
                login_status: callAccount === null || callAccount === void 0 ? void 0 : callAccount.isLogin,
                login_time: new Date(),
                cookie: callAccount.cookie,
                update_time: new Date(),
                vip_status: vipStatus,
                vip_type: vipType,
                current_level: (_a = callAccount.levelInfo) === null || _a === void 0 ? void 0 : _a.currentLevel,
                current_exp: (_b = callAccount.levelInfo) === null || _b === void 0 ? void 0 : _b.currentExp,
                next_exp: (_c = callAccount.levelInfo) === null || _c === void 0 ? void 0 : _c.nextExp
            });
            await biliAccountService.update(biliAccount);
        }
        else {
            biliAccount = new biliAccount_2.BiliAccount({
                mid: callAccount.mid,
                name: callAccount.uname,
                avatar: callAccount.face,
                login_status: callAccount === null || callAccount === void 0 ? void 0 : callAccount.isLogin,
                login_time: new Date(),
                cookie: callAccount.cookie,
                create_time: new Date(),
                vip_status: vipStatus,
                vip_type: vipType,
                current_level: (_d = callAccount.levelInfo) === null || _d === void 0 ? void 0 : _d.currentLevel,
                current_exp: (_e = callAccount.levelInfo) === null || _e === void 0 ? void 0 : _e.currentExp,
                next_exp: (_f = callAccount.levelInfo) === null || _f === void 0 ? void 0 : _f.nextExp
            });
            await biliAccountService.create(biliAccount);
        }
        callback(null, { reply: "Reply from BiliBotServer..." });
    }
};
exports.syncBiliAccount = syncBiliAccount;
//# sourceMappingURL=sync_bili_account.js.map