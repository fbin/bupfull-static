"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const grpc_js_1 = require("@grpc/grpc-js");
const bili_bot_services_1 = require("../protos/bili_bot_services");
const sync_bili_account_1 = require("./sync_bili_account");
const sync_bili_task_1 = require("./sync_bili_task");
const config_1 = __importDefault(require("../config"));
const logger_1 = __importDefault(require("../loaders/logger"));
const typedi_1 = require("typedi");
const server = new grpc_js_1.Server();
server.addService(bili_bot_services_1.BiliBotService, { syncBiliAccount: sync_bili_account_1.syncBiliAccount, syncBiliTask: sync_bili_task_1.syncBiliTask });
server.bindAsync(`localhost:${config_1.default.botPort}`, grpc_js_1.ServerCredentials.createInsecure(), (err, port) => {
    var _a;
    if (err) {
        throw err;
    }
    server.start();
    typedi_1.Container.set('logger', logger_1.default);
    logger_1.default.debug(`✌️ BiliBot服务启动成功！` + port);
    (_a = process.send) === null || _a === void 0 ? void 0 : _a.call(process, 'ready go');
});
//# sourceMappingURL=index.js.map