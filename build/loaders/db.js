"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importDefault(require("./logger"));
const env_1 = require("../data/models/env");
const cron_1 = require("../data/models/cron");
const dependence_1 = require("../data/models/dependence");
const open_1 = require("../data/models/open");
const auth_1 = require("../data/models/auth");
const subscription_1 = require("../data/models/subscription");
const cronView_1 = require("../data/models/cronView");
const sysUser_1 = require("../data/models/sysUser");
const sysUserRole_1 = require("../data/models/sysUserRole");
const sysRole_1 = require("../data/models/sysRole");
const sysRolePrivilege_1 = require("../data/models/sysRolePrivilege");
const sysMenu_1 = require("../data/models/sysMenu");
const sysFunc_1 = require("../data/models/sysFunc");
const sysApi_1 = require("../data/models/sysApi");
const sysOperateLog_1 = require("../data/models/sysOperateLog");
const sysConfig_1 = require("../data/models/sysConfig");
const biliAccount_1 = require("../data/models/biliAccount");
const biliUpAccount_1 = require("../data/models/biliUpAccount");
const biliAccountBindProxyIpRecord_1 = require("../data/models/biliAccountBindProxyIpRecord");
const cityDistributeTemplate_1 = require("../data/models/cityDistributeTemplate");
const cityDistributeTemplateItem_1 = require("../data/models/cityDistributeTemplateItem");
const proxyIp_1 = require("../data/models/proxyIp");
const commentTask_1 = require("../data/models/commentTask");
const commentTaskExecuteRecord_1 = require("../data/models/commentTaskExecuteRecord");
const commentTaskExecuteItem_1 = require("../data/models/commentTaskExecuteItem");
const simpleTask_1 = require("../data/models/simpleTask");
const simpleTaskExecuteRecord_1 = require("../data/models/simpleTaskExecuteRecord");
const simpleTaskExecuteItem_1 = require("../data/models/simpleTaskExecuteItem");
const simpleTaskExecuteStatistic_1 = require("../data/models/simpleTaskExecuteStatistic");
const commentTaskExecuteStatistic_1 = require("../data/models/commentTaskExecuteStatistic");
const commentPool_1 = require("../data/models/commentPool");
const tag_1 = require("../data/models/tag");
const biliAccountTag_1 = require("../data/models/biliAccountTag");
exports.default = async () => {
    try {
        await cron_1.CrontabModel.sync();
        await dependence_1.DependenceModel.sync();
        await open_1.AppModel.sync();
        await auth_1.AuthModel.sync();
        await env_1.EnvModel.sync();
        await subscription_1.SubscriptionModel.sync();
        await cronView_1.CrontabViewModel.sync();
        // 系统表
        await sysUser_1.SysUserModel.sync();
        await sysUserRole_1.SysUserRoleModel.sync();
        await sysRole_1.SysRoleModel.sync();
        await sysRolePrivilege_1.SysRolePrivilegeModel.sync();
        await sysMenu_1.SysMenuModel.sync();
        await sysFunc_1.SysFuncModel.sync();
        await sysApi_1.SysApiModel.sync();
        await sysOperateLog_1.SysOperateLogModel.sync();
        await sysConfig_1.SysConfigModel.sync();
        // 业务表
        await biliAccount_1.BiliAccountModel.sync();
        await biliUpAccount_1.BiliUpAccountModel.sync();
        await biliAccountBindProxyIpRecord_1.BiliAccountBindProxyIpRecordModel.sync();
        await cityDistributeTemplate_1.CityDistributeTemplateModel.sync();
        await cityDistributeTemplateItem_1.CityDistributeTemplateItemModel.sync();
        await proxyIp_1.ProxyIpModel.sync();
        await commentTask_1.CommentTaskModel.sync();
        await commentTaskExecuteRecord_1.CommentTaskExecuteRecordModel.sync();
        await commentTaskExecuteItem_1.CommentTaskExecuteItemModel.sync();
        await simpleTask_1.SimpleTaskModel.sync();
        await simpleTaskExecuteRecord_1.SimpleTaskExecuteRecordModel.sync();
        await simpleTaskExecuteItem_1.SimpleTaskExecuteItemModel.sync();
        await simpleTaskExecuteStatistic_1.SimpleTaskExecuteStatisticModel.sync();
        await commentTaskExecuteStatistic_1.CommentTaskExecuteStatisticModel.sync();
        await tag_1.TagModel.sync();
        await biliAccountTag_1.BiliAccountTagModel.sync();
        await commentPool_1.CommentPoolModel.sync();
        logger_1.default.info('✌️ DB loaded');
    }
    catch (error) {
        logger_1.default.info('✌️ DB load failed');
        logger_1.default.info(error);
    }
};
//# sourceMappingURL=db.js.map