"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BiliBotClient = exports.BiliBotService = exports.BiliTaskResponse = exports.BiliTaskRequest = exports.SyncBiliAccountResponse = exports.SyncBiliAccountRequest = exports.TaskResult = exports.BiliTask_CommentController = exports.BiliTask_WhiteList = exports.BiliTask_Comment = exports.BiliTask_Carrier = exports.BiliTask_BlCookie = exports.BiliTask_PayLoad = exports.BiliTask = exports.BiliAccount_LevelInfo = exports.BiliAccount = exports.taskResult_TaskStatusToJSON = exports.taskResult_TaskStatusFromJSON = exports.TaskResult_TaskStatus = exports.biliAccount_VipStatusToJSON = exports.biliAccount_VipStatusFromJSON = exports.BiliAccount_VipStatus = exports.biliAccount_VipTypeToJSON = exports.biliAccount_VipTypeFromJSON = exports.BiliAccount_VipType = exports.protobufPackage = void 0;
/* eslint-disable */
const grpc_js_1 = require("@grpc/grpc-js");
const minimal_1 = __importDefault(require("protobufjs/minimal"));
exports.protobufPackage = "";
var BiliAccount_VipType;
(function (BiliAccount_VipType) {
    /** None - 无 */
    BiliAccount_VipType[BiliAccount_VipType["None"] = 0] = "None";
    /** Mensual - 月度大会员 */
    BiliAccount_VipType[BiliAccount_VipType["Mensual"] = 1] = "Mensual";
    /** Annual - 年度大会员 */
    BiliAccount_VipType[BiliAccount_VipType["Annual"] = 2] = "Annual";
    BiliAccount_VipType[BiliAccount_VipType["UNRECOGNIZED"] = -1] = "UNRECOGNIZED";
})(BiliAccount_VipType = exports.BiliAccount_VipType || (exports.BiliAccount_VipType = {}));
function biliAccount_VipTypeFromJSON(object) {
    switch (object) {
        case 0:
        case "None":
            return BiliAccount_VipType.None;
        case 1:
        case "Mensual":
            return BiliAccount_VipType.Mensual;
        case 2:
        case "Annual":
            return BiliAccount_VipType.Annual;
        case -1:
        case "UNRECOGNIZED":
        default:
            return BiliAccount_VipType.UNRECOGNIZED;
    }
}
exports.biliAccount_VipTypeFromJSON = biliAccount_VipTypeFromJSON;
function biliAccount_VipTypeToJSON(object) {
    switch (object) {
        case BiliAccount_VipType.None:
            return "None";
        case BiliAccount_VipType.Mensual:
            return "Mensual";
        case BiliAccount_VipType.Annual:
            return "Annual";
        case BiliAccount_VipType.UNRECOGNIZED:
        default:
            return "UNRECOGNIZED";
    }
}
exports.biliAccount_VipTypeToJSON = biliAccount_VipTypeToJSON;
var BiliAccount_VipStatus;
(function (BiliAccount_VipStatus) {
    /** Disable - 无/过期 */
    BiliAccount_VipStatus[BiliAccount_VipStatus["Disable"] = 0] = "Disable";
    /** Enable - 正常 */
    BiliAccount_VipStatus[BiliAccount_VipStatus["Enable"] = 1] = "Enable";
    BiliAccount_VipStatus[BiliAccount_VipStatus["UNRECOGNIZED"] = -1] = "UNRECOGNIZED";
})(BiliAccount_VipStatus = exports.BiliAccount_VipStatus || (exports.BiliAccount_VipStatus = {}));
function biliAccount_VipStatusFromJSON(object) {
    switch (object) {
        case 0:
        case "Disable":
            return BiliAccount_VipStatus.Disable;
        case 1:
        case "Enable":
            return BiliAccount_VipStatus.Enable;
        case -1:
        case "UNRECOGNIZED":
        default:
            return BiliAccount_VipStatus.UNRECOGNIZED;
    }
}
exports.biliAccount_VipStatusFromJSON = biliAccount_VipStatusFromJSON;
function biliAccount_VipStatusToJSON(object) {
    switch (object) {
        case BiliAccount_VipStatus.Disable:
            return "Disable";
        case BiliAccount_VipStatus.Enable:
            return "Enable";
        case BiliAccount_VipStatus.UNRECOGNIZED:
        default:
            return "UNRECOGNIZED";
    }
}
exports.biliAccount_VipStatusToJSON = biliAccount_VipStatusToJSON;
var TaskResult_TaskStatus;
(function (TaskResult_TaskStatus) {
    TaskResult_TaskStatus[TaskResult_TaskStatus["Pending"] = 0] = "Pending";
    TaskResult_TaskStatus[TaskResult_TaskStatus["Running"] = 1] = "Running";
    TaskResult_TaskStatus[TaskResult_TaskStatus["Success"] = 2] = "Success";
    TaskResult_TaskStatus[TaskResult_TaskStatus["Failed"] = 3] = "Failed";
    TaskResult_TaskStatus[TaskResult_TaskStatus["UNRECOGNIZED"] = -1] = "UNRECOGNIZED";
})(TaskResult_TaskStatus = exports.TaskResult_TaskStatus || (exports.TaskResult_TaskStatus = {}));
function taskResult_TaskStatusFromJSON(object) {
    switch (object) {
        case 0:
        case "Pending":
            return TaskResult_TaskStatus.Pending;
        case 1:
        case "Running":
            return TaskResult_TaskStatus.Running;
        case 2:
        case "Success":
            return TaskResult_TaskStatus.Success;
        case 3:
        case "Failed":
            return TaskResult_TaskStatus.Failed;
        case -1:
        case "UNRECOGNIZED":
        default:
            return TaskResult_TaskStatus.UNRECOGNIZED;
    }
}
exports.taskResult_TaskStatusFromJSON = taskResult_TaskStatusFromJSON;
function taskResult_TaskStatusToJSON(object) {
    switch (object) {
        case TaskResult_TaskStatus.Pending:
            return "Pending";
        case TaskResult_TaskStatus.Running:
            return "Running";
        case TaskResult_TaskStatus.Success:
            return "Success";
        case TaskResult_TaskStatus.Failed:
            return "Failed";
        case TaskResult_TaskStatus.UNRECOGNIZED:
        default:
            return "UNRECOGNIZED";
    }
}
exports.taskResult_TaskStatusToJSON = taskResult_TaskStatusToJSON;
function createBaseBiliAccount() {
    return {
        isLogin: false,
        face: "",
        mid: "",
        money: "",
        uname: "",
        cookie: "",
        levelInfo: undefined,
        vipType: 0,
        vipStatus: 0,
    };
}
exports.BiliAccount = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.isLogin === true) {
            writer.uint32(8).bool(message.isLogin);
        }
        if (message.face !== "") {
            writer.uint32(18).string(message.face);
        }
        if (message.mid !== "") {
            writer.uint32(26).string(message.mid);
        }
        if (message.money !== "") {
            writer.uint32(34).string(message.money);
        }
        if (message.uname !== "") {
            writer.uint32(42).string(message.uname);
        }
        if (message.cookie !== "") {
            writer.uint32(50).string(message.cookie);
        }
        if (message.levelInfo !== undefined) {
            exports.BiliAccount_LevelInfo.encode(message.levelInfo, writer.uint32(58).fork()).ldelim();
        }
        if (message.vipType !== 0) {
            writer.uint32(64).int32(message.vipType);
        }
        if (message.vipStatus !== 0) {
            writer.uint32(72).int32(message.vipStatus);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliAccount();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 8) {
                        break;
                    }
                    message.isLogin = reader.bool();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.face = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.mid = reader.string();
                    continue;
                case 4:
                    if (tag !== 34) {
                        break;
                    }
                    message.money = reader.string();
                    continue;
                case 5:
                    if (tag !== 42) {
                        break;
                    }
                    message.uname = reader.string();
                    continue;
                case 6:
                    if (tag !== 50) {
                        break;
                    }
                    message.cookie = reader.string();
                    continue;
                case 7:
                    if (tag !== 58) {
                        break;
                    }
                    message.levelInfo = exports.BiliAccount_LevelInfo.decode(reader, reader.uint32());
                    continue;
                case 8:
                    if (tag !== 64) {
                        break;
                    }
                    message.vipType = reader.int32();
                    continue;
                case 9:
                    if (tag !== 72) {
                        break;
                    }
                    message.vipStatus = reader.int32();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            isLogin: isSet(object.isLogin) ? Boolean(object.isLogin) : false,
            face: isSet(object.face) ? String(object.face) : "",
            mid: isSet(object.mid) ? String(object.mid) : "",
            money: isSet(object.money) ? String(object.money) : "",
            uname: isSet(object.uname) ? String(object.uname) : "",
            cookie: isSet(object.cookie) ? String(object.cookie) : "",
            levelInfo: isSet(object.levelInfo) ? exports.BiliAccount_LevelInfo.fromJSON(object.levelInfo) : undefined,
            vipType: isSet(object.vipType) ? biliAccount_VipTypeFromJSON(object.vipType) : 0,
            vipStatus: isSet(object.vipStatus) ? biliAccount_VipStatusFromJSON(object.vipStatus) : 0,
        };
    },
    toJSON(message) {
        const obj = {};
        message.isLogin !== undefined && (obj.isLogin = message.isLogin);
        message.face !== undefined && (obj.face = message.face);
        message.mid !== undefined && (obj.mid = message.mid);
        message.money !== undefined && (obj.money = message.money);
        message.uname !== undefined && (obj.uname = message.uname);
        message.cookie !== undefined && (obj.cookie = message.cookie);
        message.levelInfo !== undefined &&
            (obj.levelInfo = message.levelInfo ? exports.BiliAccount_LevelInfo.toJSON(message.levelInfo) : undefined);
        message.vipType !== undefined && (obj.vipType = biliAccount_VipTypeToJSON(message.vipType));
        message.vipStatus !== undefined && (obj.vipStatus = biliAccount_VipStatusToJSON(message.vipStatus));
        return obj;
    },
    create(base) {
        return exports.BiliAccount.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c, _d, _e, _f, _g, _h;
        const message = createBaseBiliAccount();
        message.isLogin = (_a = object.isLogin) !== null && _a !== void 0 ? _a : false;
        message.face = (_b = object.face) !== null && _b !== void 0 ? _b : "";
        message.mid = (_c = object.mid) !== null && _c !== void 0 ? _c : "";
        message.money = (_d = object.money) !== null && _d !== void 0 ? _d : "";
        message.uname = (_e = object.uname) !== null && _e !== void 0 ? _e : "";
        message.cookie = (_f = object.cookie) !== null && _f !== void 0 ? _f : "";
        message.levelInfo = (object.levelInfo !== undefined && object.levelInfo !== null)
            ? exports.BiliAccount_LevelInfo.fromPartial(object.levelInfo)
            : undefined;
        message.vipType = (_g = object.vipType) !== null && _g !== void 0 ? _g : 0;
        message.vipStatus = (_h = object.vipStatus) !== null && _h !== void 0 ? _h : 0;
        return message;
    },
};
function createBaseBiliAccount_LevelInfo() {
    return { currentLevel: 0, currentMin: 0, currentExp: 0, nextExp: 0 };
}
exports.BiliAccount_LevelInfo = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.currentLevel !== 0) {
            writer.uint32(8).int32(message.currentLevel);
        }
        if (message.currentMin !== 0) {
            writer.uint32(16).int32(message.currentMin);
        }
        if (message.currentExp !== 0) {
            writer.uint32(24).int32(message.currentExp);
        }
        if (message.nextExp !== 0) {
            writer.uint32(32).int32(message.nextExp);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliAccount_LevelInfo();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 8) {
                        break;
                    }
                    message.currentLevel = reader.int32();
                    continue;
                case 2:
                    if (tag !== 16) {
                        break;
                    }
                    message.currentMin = reader.int32();
                    continue;
                case 3:
                    if (tag !== 24) {
                        break;
                    }
                    message.currentExp = reader.int32();
                    continue;
                case 4:
                    if (tag !== 32) {
                        break;
                    }
                    message.nextExp = reader.int32();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            currentLevel: isSet(object.currentLevel) ? Number(object.currentLevel) : 0,
            currentMin: isSet(object.currentMin) ? Number(object.currentMin) : 0,
            currentExp: isSet(object.currentExp) ? Number(object.currentExp) : 0,
            nextExp: isSet(object.nextExp) ? Number(object.nextExp) : 0,
        };
    },
    toJSON(message) {
        const obj = {};
        message.currentLevel !== undefined && (obj.currentLevel = Math.round(message.currentLevel));
        message.currentMin !== undefined && (obj.currentMin = Math.round(message.currentMin));
        message.currentExp !== undefined && (obj.currentExp = Math.round(message.currentExp));
        message.nextExp !== undefined && (obj.nextExp = Math.round(message.nextExp));
        return obj;
    },
    create(base) {
        return exports.BiliAccount_LevelInfo.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c, _d;
        const message = createBaseBiliAccount_LevelInfo();
        message.currentLevel = (_a = object.currentLevel) !== null && _a !== void 0 ? _a : 0;
        message.currentMin = (_b = object.currentMin) !== null && _b !== void 0 ? _b : 0;
        message.currentExp = (_c = object.currentExp) !== null && _c !== void 0 ? _c : 0;
        message.nextExp = (_d = object.nextExp) !== null && _d !== void 0 ? _d : 0;
        return message;
    },
};
function createBaseBiliTask() {
    return { uid: "", bvid: "", recordId: "", runTasks: "", payLoad: undefined };
}
exports.BiliTask = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.uid !== "") {
            writer.uint32(10).string(message.uid);
        }
        if (message.bvid !== "") {
            writer.uint32(18).string(message.bvid);
        }
        if (message.recordId !== "") {
            writer.uint32(26).string(message.recordId);
        }
        if (message.runTasks !== "") {
            writer.uint32(34).string(message.runTasks);
        }
        if (message.payLoad !== undefined) {
            exports.BiliTask_PayLoad.encode(message.payLoad, writer.uint32(42).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTask();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.uid = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.bvid = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.recordId = reader.string();
                    continue;
                case 4:
                    if (tag !== 34) {
                        break;
                    }
                    message.runTasks = reader.string();
                    continue;
                case 5:
                    if (tag !== 42) {
                        break;
                    }
                    message.payLoad = exports.BiliTask_PayLoad.decode(reader, reader.uint32());
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            uid: isSet(object.uid) ? String(object.uid) : "",
            bvid: isSet(object.bvid) ? String(object.bvid) : "",
            recordId: isSet(object.recordId) ? String(object.recordId) : "",
            runTasks: isSet(object.runTasks) ? String(object.runTasks) : "",
            payLoad: isSet(object.payLoad) ? exports.BiliTask_PayLoad.fromJSON(object.payLoad) : undefined,
        };
    },
    toJSON(message) {
        const obj = {};
        message.uid !== undefined && (obj.uid = message.uid);
        message.bvid !== undefined && (obj.bvid = message.bvid);
        message.recordId !== undefined && (obj.recordId = message.recordId);
        message.runTasks !== undefined && (obj.runTasks = message.runTasks);
        message.payLoad !== undefined &&
            (obj.payLoad = message.payLoad ? exports.BiliTask_PayLoad.toJSON(message.payLoad) : undefined);
        return obj;
    },
    create(base) {
        return exports.BiliTask.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c, _d;
        const message = createBaseBiliTask();
        message.uid = (_a = object.uid) !== null && _a !== void 0 ? _a : "";
        message.bvid = (_b = object.bvid) !== null && _b !== void 0 ? _b : "";
        message.recordId = (_c = object.recordId) !== null && _c !== void 0 ? _c : "";
        message.runTasks = (_d = object.runTasks) !== null && _d !== void 0 ? _d : "";
        message.payLoad = (object.payLoad !== undefined && object.payLoad !== null)
            ? exports.BiliTask_PayLoad.fromPartial(object.payLoad)
            : undefined;
        return message;
    },
};
function createBaseBiliTask_PayLoad() {
    return { type: "", extra: "", blCookies: [] };
}
exports.BiliTask_PayLoad = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.type !== "") {
            writer.uint32(10).string(message.type);
        }
        if (message.extra !== "") {
            writer.uint32(18).string(message.extra);
        }
        for (const v of message.blCookies) {
            exports.BiliTask_BlCookie.encode(v, writer.uint32(26).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTask_PayLoad();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.type = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.extra = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.blCookies.push(exports.BiliTask_BlCookie.decode(reader, reader.uint32()));
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            type: isSet(object.type) ? String(object.type) : "",
            extra: isSet(object.extra) ? String(object.extra) : "",
            blCookies: Array.isArray(object === null || object === void 0 ? void 0 : object.blCookies)
                ? object.blCookies.map((e) => exports.BiliTask_BlCookie.fromJSON(e))
                : [],
        };
    },
    toJSON(message) {
        const obj = {};
        message.type !== undefined && (obj.type = message.type);
        message.extra !== undefined && (obj.extra = message.extra);
        if (message.blCookies) {
            obj.blCookies = message.blCookies.map((e) => e ? exports.BiliTask_BlCookie.toJSON(e) : undefined);
        }
        else {
            obj.blCookies = [];
        }
        return obj;
    },
    create(base) {
        return exports.BiliTask_PayLoad.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c;
        const message = createBaseBiliTask_PayLoad();
        message.type = (_a = object.type) !== null && _a !== void 0 ? _a : "";
        message.extra = (_b = object.extra) !== null && _b !== void 0 ? _b : "";
        message.blCookies = ((_c = object.blCookies) === null || _c === void 0 ? void 0 : _c.map((e) => exports.BiliTask_BlCookie.fromPartial(e))) || [];
        return message;
    },
};
function createBaseBiliTask_BlCookie() {
    return { snapId: "", mid: "", proxyIp: "", cookie: "", ua: "", carrier: undefined };
}
exports.BiliTask_BlCookie = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.snapId !== "") {
            writer.uint32(10).string(message.snapId);
        }
        if (message.mid !== "") {
            writer.uint32(18).string(message.mid);
        }
        if (message.proxyIp !== "") {
            writer.uint32(26).string(message.proxyIp);
        }
        if (message.cookie !== "") {
            writer.uint32(34).string(message.cookie);
        }
        if (message.ua !== "") {
            writer.uint32(42).string(message.ua);
        }
        if (message.carrier !== undefined) {
            exports.BiliTask_Carrier.encode(message.carrier, writer.uint32(50).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTask_BlCookie();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.snapId = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.mid = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.proxyIp = reader.string();
                    continue;
                case 4:
                    if (tag !== 34) {
                        break;
                    }
                    message.cookie = reader.string();
                    continue;
                case 5:
                    if (tag !== 42) {
                        break;
                    }
                    message.ua = reader.string();
                    continue;
                case 6:
                    if (tag !== 50) {
                        break;
                    }
                    message.carrier = exports.BiliTask_Carrier.decode(reader, reader.uint32());
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            snapId: isSet(object.snapId) ? String(object.snapId) : "",
            mid: isSet(object.mid) ? String(object.mid) : "",
            proxyIp: isSet(object.proxyIp) ? String(object.proxyIp) : "",
            cookie: isSet(object.cookie) ? String(object.cookie) : "",
            ua: isSet(object.ua) ? String(object.ua) : "",
            carrier: isSet(object.carrier) ? exports.BiliTask_Carrier.fromJSON(object.carrier) : undefined,
        };
    },
    toJSON(message) {
        const obj = {};
        message.snapId !== undefined && (obj.snapId = message.snapId);
        message.mid !== undefined && (obj.mid = message.mid);
        message.proxyIp !== undefined && (obj.proxyIp = message.proxyIp);
        message.cookie !== undefined && (obj.cookie = message.cookie);
        message.ua !== undefined && (obj.ua = message.ua);
        message.carrier !== undefined &&
            (obj.carrier = message.carrier ? exports.BiliTask_Carrier.toJSON(message.carrier) : undefined);
        return obj;
    },
    create(base) {
        return exports.BiliTask_BlCookie.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c, _d, _e;
        const message = createBaseBiliTask_BlCookie();
        message.snapId = (_a = object.snapId) !== null && _a !== void 0 ? _a : "";
        message.mid = (_b = object.mid) !== null && _b !== void 0 ? _b : "";
        message.proxyIp = (_c = object.proxyIp) !== null && _c !== void 0 ? _c : "";
        message.cookie = (_d = object.cookie) !== null && _d !== void 0 ? _d : "";
        message.ua = (_e = object.ua) !== null && _e !== void 0 ? _e : "";
        message.carrier = (object.carrier !== undefined && object.carrier !== null)
            ? exports.BiliTask_Carrier.fromPartial(object.carrier)
            : undefined;
        return message;
    },
};
function createBaseBiliTask_Carrier() {
    return { comments: [], commentController: undefined };
}
exports.BiliTask_Carrier = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        for (const v of message.comments) {
            exports.BiliTask_Comment.encode(v, writer.uint32(10).fork()).ldelim();
        }
        if (message.commentController !== undefined) {
            exports.BiliTask_CommentController.encode(message.commentController, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTask_Carrier();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.comments.push(exports.BiliTask_Comment.decode(reader, reader.uint32()));
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.commentController = exports.BiliTask_CommentController.decode(reader, reader.uint32());
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            comments: Array.isArray(object === null || object === void 0 ? void 0 : object.comments) ? object.comments.map((e) => exports.BiliTask_Comment.fromJSON(e)) : [],
            commentController: isSet(object.commentController)
                ? exports.BiliTask_CommentController.fromJSON(object.commentController)
                : undefined,
        };
    },
    toJSON(message) {
        const obj = {};
        if (message.comments) {
            obj.comments = message.comments.map((e) => e ? exports.BiliTask_Comment.toJSON(e) : undefined);
        }
        else {
            obj.comments = [];
        }
        message.commentController !== undefined && (obj.commentController = message.commentController
            ? exports.BiliTask_CommentController.toJSON(message.commentController)
            : undefined);
        return obj;
    },
    create(base) {
        return exports.BiliTask_Carrier.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a;
        const message = createBaseBiliTask_Carrier();
        message.comments = ((_a = object.comments) === null || _a === void 0 ? void 0 : _a.map((e) => exports.BiliTask_Comment.fromPartial(e))) || [];
        message.commentController = (object.commentController !== undefined && object.commentController !== null)
            ? exports.BiliTask_CommentController.fromPartial(object.commentController)
            : undefined;
        return message;
    },
};
function createBaseBiliTask_Comment() {
    return { id: "", content: "" };
}
exports.BiliTask_Comment = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.id !== "") {
            writer.uint32(10).string(message.id);
        }
        if (message.content !== "") {
            writer.uint32(18).string(message.content);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTask_Comment();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.id = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.content = reader.string();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            id: isSet(object.id) ? String(object.id) : "",
            content: isSet(object.content) ? String(object.content) : "",
        };
    },
    toJSON(message) {
        const obj = {};
        message.id !== undefined && (obj.id = message.id);
        message.content !== undefined && (obj.content = message.content);
        return obj;
    },
    create(base) {
        return exports.BiliTask_Comment.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b;
        const message = createBaseBiliTask_Comment();
        message.id = (_a = object.id) !== null && _a !== void 0 ? _a : "";
        message.content = (_b = object.content) !== null && _b !== void 0 ? _b : "";
        return message;
    },
};
function createBaseBiliTask_WhiteList() {
    return { mid: "" };
}
exports.BiliTask_WhiteList = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.mid !== "") {
            writer.uint32(10).string(message.mid);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTask_WhiteList();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.mid = reader.string();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return { mid: isSet(object.mid) ? String(object.mid) : "" };
    },
    toJSON(message) {
        const obj = {};
        message.mid !== undefined && (obj.mid = message.mid);
        return obj;
    },
    create(base) {
        return exports.BiliTask_WhiteList.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a;
        const message = createBaseBiliTask_WhiteList();
        message.mid = (_a = object.mid) !== null && _a !== void 0 ? _a : "";
        return message;
    },
};
function createBaseBiliTask_CommentController() {
    return { controlTime: "", whiteList: [] };
}
exports.BiliTask_CommentController = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.controlTime !== "") {
            writer.uint32(10).string(message.controlTime);
        }
        for (const v of message.whiteList) {
            exports.BiliTask_WhiteList.encode(v, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTask_CommentController();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.controlTime = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.whiteList.push(exports.BiliTask_WhiteList.decode(reader, reader.uint32()));
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            controlTime: isSet(object.controlTime) ? String(object.controlTime) : "",
            whiteList: Array.isArray(object === null || object === void 0 ? void 0 : object.whiteList)
                ? object.whiteList.map((e) => exports.BiliTask_WhiteList.fromJSON(e))
                : [],
        };
    },
    toJSON(message) {
        const obj = {};
        message.controlTime !== undefined && (obj.controlTime = message.controlTime);
        if (message.whiteList) {
            obj.whiteList = message.whiteList.map((e) => e ? exports.BiliTask_WhiteList.toJSON(e) : undefined);
        }
        else {
            obj.whiteList = [];
        }
        return obj;
    },
    create(base) {
        return exports.BiliTask_CommentController.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b;
        const message = createBaseBiliTask_CommentController();
        message.controlTime = (_a = object.controlTime) !== null && _a !== void 0 ? _a : "";
        message.whiteList = ((_b = object.whiteList) === null || _b === void 0 ? void 0 : _b.map((e) => exports.BiliTask_WhiteList.fromPartial(e))) || [];
        return message;
    },
};
function createBaseTaskResult() {
    return { mid: "", snapId: "", recordId: "", taskStatus: 0 };
}
exports.TaskResult = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.mid !== "") {
            writer.uint32(10).string(message.mid);
        }
        if (message.snapId !== "") {
            writer.uint32(18).string(message.snapId);
        }
        if (message.recordId !== "") {
            writer.uint32(26).string(message.recordId);
        }
        if (message.taskStatus !== 0) {
            writer.uint32(32).int32(message.taskStatus);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseTaskResult();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.mid = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.snapId = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.recordId = reader.string();
                    continue;
                case 4:
                    if (tag !== 32) {
                        break;
                    }
                    message.taskStatus = reader.int32();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            mid: isSet(object.mid) ? String(object.mid) : "",
            snapId: isSet(object.snapId) ? String(object.snapId) : "",
            recordId: isSet(object.recordId) ? String(object.recordId) : "",
            taskStatus: isSet(object.taskStatus) ? taskResult_TaskStatusFromJSON(object.taskStatus) : 0,
        };
    },
    toJSON(message) {
        const obj = {};
        message.mid !== undefined && (obj.mid = message.mid);
        message.snapId !== undefined && (obj.snapId = message.snapId);
        message.recordId !== undefined && (obj.recordId = message.recordId);
        message.taskStatus !== undefined && (obj.taskStatus = taskResult_TaskStatusToJSON(message.taskStatus));
        return obj;
    },
    create(base) {
        return exports.TaskResult.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c, _d;
        const message = createBaseTaskResult();
        message.mid = (_a = object.mid) !== null && _a !== void 0 ? _a : "";
        message.snapId = (_b = object.snapId) !== null && _b !== void 0 ? _b : "";
        message.recordId = (_c = object.recordId) !== null && _c !== void 0 ? _c : "";
        message.taskStatus = (_d = object.taskStatus) !== null && _d !== void 0 ? _d : 0;
        return message;
    },
};
function createBaseSyncBiliAccountRequest() {
    return { biliAccount: undefined };
}
exports.SyncBiliAccountRequest = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.biliAccount !== undefined) {
            exports.BiliAccount.encode(message.biliAccount, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseSyncBiliAccountRequest();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.biliAccount = exports.BiliAccount.decode(reader, reader.uint32());
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return { biliAccount: isSet(object.biliAccount) ? exports.BiliAccount.fromJSON(object.biliAccount) : undefined };
    },
    toJSON(message) {
        const obj = {};
        message.biliAccount !== undefined &&
            (obj.biliAccount = message.biliAccount ? exports.BiliAccount.toJSON(message.biliAccount) : undefined);
        return obj;
    },
    create(base) {
        return exports.SyncBiliAccountRequest.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        const message = createBaseSyncBiliAccountRequest();
        message.biliAccount = (object.biliAccount !== undefined && object.biliAccount !== null)
            ? exports.BiliAccount.fromPartial(object.biliAccount)
            : undefined;
        return message;
    },
};
function createBaseSyncBiliAccountResponse() {
    return { reply: "" };
}
exports.SyncBiliAccountResponse = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.reply !== "") {
            writer.uint32(10).string(message.reply);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseSyncBiliAccountResponse();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.reply = reader.string();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return { reply: isSet(object.reply) ? String(object.reply) : "" };
    },
    toJSON(message) {
        const obj = {};
        message.reply !== undefined && (obj.reply = message.reply);
        return obj;
    },
    create(base) {
        return exports.SyncBiliAccountResponse.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a;
        const message = createBaseSyncBiliAccountResponse();
        message.reply = (_a = object.reply) !== null && _a !== void 0 ? _a : "";
        return message;
    },
};
function createBaseBiliTaskRequest() {
    return { recordId: "", runTasks: "", taskResult: undefined, completed: false };
}
exports.BiliTaskRequest = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.recordId !== "") {
            writer.uint32(10).string(message.recordId);
        }
        if (message.runTasks !== "") {
            writer.uint32(18).string(message.runTasks);
        }
        if (message.taskResult !== undefined) {
            exports.TaskResult.encode(message.taskResult, writer.uint32(26).fork()).ldelim();
        }
        if (message.completed === true) {
            writer.uint32(32).bool(message.completed);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTaskRequest();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.recordId = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.runTasks = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.taskResult = exports.TaskResult.decode(reader, reader.uint32());
                    continue;
                case 4:
                    if (tag !== 32) {
                        break;
                    }
                    message.completed = reader.bool();
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            recordId: isSet(object.recordId) ? String(object.recordId) : "",
            runTasks: isSet(object.runTasks) ? String(object.runTasks) : "",
            taskResult: isSet(object.taskResult) ? exports.TaskResult.fromJSON(object.taskResult) : undefined,
            completed: isSet(object.completed) ? Boolean(object.completed) : false,
        };
    },
    toJSON(message) {
        const obj = {};
        message.recordId !== undefined && (obj.recordId = message.recordId);
        message.runTasks !== undefined && (obj.runTasks = message.runTasks);
        message.taskResult !== undefined &&
            (obj.taskResult = message.taskResult ? exports.TaskResult.toJSON(message.taskResult) : undefined);
        message.completed !== undefined && (obj.completed = message.completed);
        return obj;
    },
    create(base) {
        return exports.BiliTaskRequest.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b, _c;
        const message = createBaseBiliTaskRequest();
        message.recordId = (_a = object.recordId) !== null && _a !== void 0 ? _a : "";
        message.runTasks = (_b = object.runTasks) !== null && _b !== void 0 ? _b : "";
        message.taskResult = (object.taskResult !== undefined && object.taskResult !== null)
            ? exports.TaskResult.fromPartial(object.taskResult)
            : undefined;
        message.completed = (_c = object.completed) !== null && _c !== void 0 ? _c : false;
        return message;
    },
};
function createBaseBiliTaskResponse() {
    return { command: "", reply: "", biliTask: undefined };
}
exports.BiliTaskResponse = {
    encode(message, writer = minimal_1.default.Writer.create()) {
        if (message.command !== "") {
            writer.uint32(10).string(message.command);
        }
        if (message.reply !== "") {
            writer.uint32(18).string(message.reply);
        }
        if (message.biliTask !== undefined) {
            exports.BiliTask.encode(message.biliTask, writer.uint32(26).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof minimal_1.default.Reader ? input : minimal_1.default.Reader.create(input);
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = createBaseBiliTaskResponse();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    if (tag !== 10) {
                        break;
                    }
                    message.command = reader.string();
                    continue;
                case 2:
                    if (tag !== 18) {
                        break;
                    }
                    message.reply = reader.string();
                    continue;
                case 3:
                    if (tag !== 26) {
                        break;
                    }
                    message.biliTask = exports.BiliTask.decode(reader, reader.uint32());
                    continue;
            }
            if ((tag & 7) === 4 || tag === 0) {
                break;
            }
            reader.skipType(tag & 7);
        }
        return message;
    },
    fromJSON(object) {
        return {
            command: isSet(object.command) ? String(object.command) : "",
            reply: isSet(object.reply) ? String(object.reply) : "",
            biliTask: isSet(object.biliTask) ? exports.BiliTask.fromJSON(object.biliTask) : undefined,
        };
    },
    toJSON(message) {
        const obj = {};
        message.command !== undefined && (obj.command = message.command);
        message.reply !== undefined && (obj.reply = message.reply);
        message.biliTask !== undefined && (obj.biliTask = message.biliTask ? exports.BiliTask.toJSON(message.biliTask) : undefined);
        return obj;
    },
    create(base) {
        return exports.BiliTaskResponse.fromPartial(base !== null && base !== void 0 ? base : {});
    },
    fromPartial(object) {
        var _a, _b;
        const message = createBaseBiliTaskResponse();
        message.command = (_a = object.command) !== null && _a !== void 0 ? _a : "";
        message.reply = (_b = object.reply) !== null && _b !== void 0 ? _b : "";
        message.biliTask = (object.biliTask !== undefined && object.biliTask !== null)
            ? exports.BiliTask.fromPartial(object.biliTask)
            : undefined;
        return message;
    },
};
exports.BiliBotService = {
    /** Unary */
    syncBiliAccount: {
        path: "/BiliBot/syncBiliAccount",
        requestStream: false,
        responseStream: false,
        requestSerialize: (value) => Buffer.from(exports.SyncBiliAccountRequest.encode(value).finish()),
        requestDeserialize: (value) => exports.SyncBiliAccountRequest.decode(value),
        responseSerialize: (value) => Buffer.from(exports.SyncBiliAccountResponse.encode(value).finish()),
        responseDeserialize: (value) => exports.SyncBiliAccountResponse.decode(value),
    },
    /** Bi-directional streaming */
    syncBiliTask: {
        path: "/BiliBot/syncBiliTask",
        requestStream: true,
        responseStream: true,
        requestSerialize: (value) => Buffer.from(exports.BiliTaskRequest.encode(value).finish()),
        requestDeserialize: (value) => exports.BiliTaskRequest.decode(value),
        responseSerialize: (value) => Buffer.from(exports.BiliTaskResponse.encode(value).finish()),
        responseDeserialize: (value) => exports.BiliTaskResponse.decode(value),
    },
};
exports.BiliBotClient = (0, grpc_js_1.makeGenericClientConstructor)(exports.BiliBotService, "BiliBot");
function isSet(value) {
    return value !== null && value !== undefined;
}
//# sourceMappingURL=bili_bot_services.js.map