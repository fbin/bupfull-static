'use strict';
const { v4: uuidV4 } = require('uuid');
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
        */
        await queryInterface.bulkInsert('tag', [{
                id: uuidV4(),
                type: 1,
                name: '每日练号',
                enable_flag: true,
                del_flag: false,
                create_time: new Date(),
                update_time: new Date(),
            }, {
                id: uuidV4(),
                type: 1,
                name: '品牌维护',
                enable_flag: true,
                del_flag: false,
                create_time: new Date(),
                update_time: new Date(),
            }]);
    },
    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        await queryInterface.bulkDelete('tag', null, {});
    }
};
//# sourceMappingURL=20230825105214-insert_tag_to_tag_table.js.map