"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sequelize = void 0;
const sequelize_1 = require("sequelize");
// export const sequelize = new Sequelize({
//   dialect: 'sqlite',
//   storage: join(config.dbPath, 'database.sqlite'),
//   logging: false,
//   retry: {
//     max: 10,
//     match: ['SQLITE_BUSY: database is locked'],
//   },
//   pool: {
//     max: 5,
//     min: 2,
//     idle: 30000,
//     acquire: 30000,
//     evict: 10000,
//   },
//   transactionType: Transaction.TYPES.IMMEDIATE,
// });
exports.sequelize = new sequelize_1.Sequelize('bup', 'root', 'root', {
    host: 'localhost',
    port: 3306,
    dialect: 'mysql',
    logging: false,
    pool: {
        max: 5,
        min: 2,
        idle: 30000,
        acquire: 30000,
        evict: 10000,
    },
    transactionType: sequelize_1.Transaction.TYPES.IMMEDIATE,
    define: {
        freezeTableName: true,
        timestamps: false,
    },
});
// 测试连接是否成功
(async () => {
    try {
        await exports.sequelize.authenticate();
        console.log('(*^▽^*) Connection has been established successfully.');
    }
    catch (error) {
        console.error('〒▽〒 Unable to connect to the database:', error);
    }
})();
//# sourceMappingURL=index.js.map