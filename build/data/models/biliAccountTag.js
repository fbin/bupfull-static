"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BiliAccountTagModel = exports.BiliAccountTypeEnum = exports.BiliAccountTag = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * B站账号标签
 */
class BiliAccountTag {
    constructor(options) {
        this.id = options.id;
        this.type = options.type;
        this.mid = options.mid;
        this.tag_id = options.tag_id;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.BiliAccountTag = BiliAccountTag;
var BiliAccountTypeEnum;
(function (BiliAccountTypeEnum) {
    BiliAccountTypeEnum[BiliAccountTypeEnum["BILI_ACCOUNT"] = 1] = "BILI_ACCOUNT";
    BiliAccountTypeEnum[BiliAccountTypeEnum["BILI_UP_ACCOUNT"] = 2] = "BILI_UP_ACCOUNT"; // B站UP主账号
})(BiliAccountTypeEnum = exports.BiliAccountTypeEnum || (exports.BiliAccountTypeEnum = {}));
exports.BiliAccountTagModel = _1.sequelize.define('bili_account_tag', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    type: sequelize_1.DataTypes.INTEGER,
    mid: sequelize_1.DataTypes.STRING(64),
    tag_id: sequelize_1.DataTypes.STRING(64),
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=biliAccountTag.js.map