"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BiliUpAccountModel = exports.BiliUpAccount = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * B站UP号
 */
class BiliUpAccount {
    constructor(options) {
        this.mid = options.mid;
        this.name = options.name;
        this.avatar = options.avatar;
        this.proxy_ip = options.proxy_ip;
        this.login_status = options.login_status;
        this.login_time = options.login_time;
        this.cookie = options.cookie;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
        this.remarks = options.remarks;
    }
}
exports.BiliUpAccount = BiliUpAccount;
exports.BiliUpAccountModel = _1.sequelize.define('bili_up_account', {
    mid: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    name: sequelize_1.DataTypes.STRING(64),
    avatar: sequelize_1.DataTypes.STRING(255),
    proxy_ip: sequelize_1.DataTypes.STRING(255),
    login_status: sequelize_1.DataTypes.BOOLEAN,
    login_time: sequelize_1.DataTypes.DATE,
    cookie: sequelize_1.DataTypes.TEXT,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
    remarks: sequelize_1.DataTypes.STRING(255),
});
//# sourceMappingURL=biliUpAccount.js.map