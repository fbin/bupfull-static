"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProxyIpModel = exports.IpTypeEnum = exports.ProxyIp = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 代理IP
 */
class ProxyIp {
    constructor(options) {
        this.ip = options.ip;
        this.ip_type = options.ip_type;
        this.city_code = options.city_code;
        this.city_name = options.city_name;
        this.isp_code = options.isp_code;
        this.isp_name = options.isp_name;
        this.effective_time = options.effective_time;
        this.expire_time = options.expire_time;
        this.enable_flag = options.enable_flag;
        this.del_flag = options.del_flag;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.ProxyIp = ProxyIp;
var IpTypeEnum;
(function (IpTypeEnum) {
    IpTypeEnum["IPV4"] = "ipv4";
    IpTypeEnum["IPV6"] = "ipv6";
})(IpTypeEnum = exports.IpTypeEnum || (exports.IpTypeEnum = {}));
exports.ProxyIpModel = _1.sequelize.define('proxy_ip', {
    ip: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(255),
    },
    ip_type: sequelize_1.DataTypes.STRING(16),
    city_code: sequelize_1.DataTypes.STRING(64),
    city_name: sequelize_1.DataTypes.STRING(64),
    isp_code: sequelize_1.DataTypes.STRING(32),
    isp_name: sequelize_1.DataTypes.STRING(64),
    effective_time: sequelize_1.DataTypes.DATE,
    expire_time: sequelize_1.DataTypes.DATE,
    enable_flag: sequelize_1.DataTypes.BOOLEAN,
    del_flag: sequelize_1.DataTypes.BOOLEAN,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=proxyIp.js.map