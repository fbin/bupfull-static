"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TagModel = exports.TagTypeEnum = exports.Tag = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 标签
 */
class Tag {
    constructor(options) {
        this.id = options.id;
        this.type = options.type;
        this.name = options.name;
        this.enable_flag = options.enable_flag;
        this.del_flag = options.del_flag;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.Tag = Tag;
var TagTypeEnum;
(function (TagTypeEnum) {
    TagTypeEnum[TagTypeEnum["BILI_ACCOUNT"] = 1] = "BILI_ACCOUNT";
    TagTypeEnum[TagTypeEnum["COMMENT_POOL"] = 2] = "COMMENT_POOL";
})(TagTypeEnum = exports.TagTypeEnum || (exports.TagTypeEnum = {}));
exports.TagModel = _1.sequelize.define('tag', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    type: sequelize_1.DataTypes.INTEGER,
    name: sequelize_1.DataTypes.STRING(64),
    enable_flag: sequelize_1.DataTypes.BOOLEAN,
    del_flag: sequelize_1.DataTypes.BOOLEAN,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=tag.js.map