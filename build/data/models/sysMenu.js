"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SysMenuModel = exports.SysMenu = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 系统菜单
 */
class SysMenu {
    constructor(options) {
        this.id = options.id;
        this.parent_id = options.parent_id;
        this.code = options.code;
        this.name = options.name;
        this.path = options.path;
        this.page_flag = options.page_flag;
        this.enable_flag = options.enable_flag;
        this.del_flag = options.del_flag;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.SysMenu = SysMenu;
exports.SysMenuModel = _1.sequelize.define('sys_menu', {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: sequelize_1.DataTypes.INTEGER,
    },
    parent_id: sequelize_1.DataTypes.INTEGER,
    code: sequelize_1.DataTypes.STRING(32),
    name: sequelize_1.DataTypes.STRING(32),
    path: sequelize_1.DataTypes.STRING(255),
    page_flag: sequelize_1.DataTypes.BOOLEAN,
    enable_flag: sequelize_1.DataTypes.BOOLEAN,
    del_flag: sequelize_1.DataTypes.BOOLEAN,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=sysMenu.js.map