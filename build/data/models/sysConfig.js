"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SysConfigModel = exports.SysConfig = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 系统配置
 */
class SysConfig {
    constructor(options) {
        this.config_key = options.config_key;
        this.config_value = options.config_value;
        this.config_label = options.config_label;
        this.enable_flag = options.enable_flag;
        this.del_flag = options.del_flag;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.SysConfig = SysConfig;
exports.SysConfigModel = _1.sequelize.define('sys_config', {
    config_key: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    config_value: sequelize_1.DataTypes.STRING(255),
    config_label: sequelize_1.DataTypes.STRING(64),
    enable_flag: sequelize_1.DataTypes.BOOLEAN,
    del_flag: sequelize_1.DataTypes.BOOLEAN,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=sysConfig.js.map