"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SysOperateLogModel = exports.OperateType = exports.SysOperateLog = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 系统操作日志
 */
class SysOperateLog {
    constructor(options) {
        this.id = options.id;
        this.type = options.type;
        this.client_ip = options.client_ip;
        this.params = options.params;
        this.resource = options.resource;
        this.result = options.result;
        this.remark = options.remark;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
    }
}
exports.SysOperateLog = SysOperateLog;
var OperateType;
(function (OperateType) {
    OperateType["ADD"] = "ADD";
    OperateType["UPDATE"] = "UPDATE";
    OperateType["DELETE"] = "DELETE";
})(OperateType = exports.OperateType || (exports.OperateType = {}));
exports.SysOperateLogModel = _1.sequelize.define('sys_operate_log', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    type: sequelize_1.DataTypes.STRING(16),
    client_ip: sequelize_1.DataTypes.STRING(255),
    params: sequelize_1.DataTypes.TEXT,
    resource: sequelize_1.DataTypes.STRING(255),
    result: sequelize_1.DataTypes.BOOLEAN,
    remark: sequelize_1.DataTypes.STRING(255),
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=sysOperateLog.js.map