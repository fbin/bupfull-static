"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommentTaskModel = exports.CommentTypeEnum = exports.CommentTaskTypeEnum = exports.CommentTask = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 评论任务
 */
class CommentTask {
    constructor(options) {
        this.id = options.id;
        this.task_type = options.task_type;
        this.uid = options.uid;
        this.bvid = options.bvid;
        this.template_code = options.template_code;
        this.execute_duration = options.execute_duration;
        this.duration_unit = options.duration_unit;
        this.comment_type = options.comment_type;
        this.comment_times = options.comment_times;
        this.comment_content = options.comment_content;
        this.comment_tag_id = options.comment_tag_id;
        this.publish_flag = options.publish_flag;
        this.publish_time = options.publish_time;
        this.publish_by = options.publish_by;
        this.publish_remark = options.publish_remark;
        this.status = options.status;
        this.enable_flag = options.enable_flag;
        this.control_flag = options.control_flag;
        this.control_time = options.control_time;
        this.mid_white_list = options.mid_white_list;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
        this.del_flag = options.del_flag;
    }
}
exports.CommentTask = CommentTask;
var CommentTaskTypeEnum;
(function (CommentTaskTypeEnum) {
    CommentTaskTypeEnum[CommentTaskTypeEnum["PUBLISH"] = 1] = "PUBLISH";
    CommentTaskTypeEnum[CommentTaskTypeEnum["CONTROL"] = 2] = "CONTROL";
})(CommentTaskTypeEnum = exports.CommentTaskTypeEnum || (exports.CommentTaskTypeEnum = {}));
var CommentTypeEnum;
(function (CommentTypeEnum) {
    CommentTypeEnum[CommentTypeEnum["BY_SORT"] = 1] = "BY_SORT";
    CommentTypeEnum[CommentTypeEnum["RAND"] = 2] = "RAND";
})(CommentTypeEnum = exports.CommentTypeEnum || (exports.CommentTypeEnum = {}));
exports.CommentTaskModel = _1.sequelize.define('comment_task', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    task_type: sequelize_1.DataTypes.INTEGER,
    uid: sequelize_1.DataTypes.STRING(64),
    bvid: sequelize_1.DataTypes.STRING(64),
    template_code: sequelize_1.DataTypes.STRING(32),
    execute_duration: sequelize_1.DataTypes.INTEGER,
    duration_unit: sequelize_1.DataTypes.STRING(4),
    comment_type: sequelize_1.DataTypes.INTEGER,
    comment_times: sequelize_1.DataTypes.INTEGER,
    comment_content: sequelize_1.DataTypes.BLOB,
    publish_flag: sequelize_1.DataTypes.BOOLEAN,
    publish_time: sequelize_1.DataTypes.DATE,
    comment_tag_id: sequelize_1.DataTypes.STRING(64),
    publish_by: sequelize_1.DataTypes.STRING(64),
    publish_remark: sequelize_1.DataTypes.STRING(64),
    status: sequelize_1.DataTypes.INTEGER,
    enable_flag: sequelize_1.DataTypes.BOOLEAN,
    control_flag: sequelize_1.DataTypes.BOOLEAN,
    control_time: sequelize_1.DataTypes.DATE,
    mid_white_list: sequelize_1.DataTypes.TEXT,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
    del_flag: sequelize_1.DataTypes.BOOLEAN,
});
//# sourceMappingURL=commentTask.js.map