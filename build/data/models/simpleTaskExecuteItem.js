"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SimpleTaskExecuteItemModel = exports.ExecuteItemStatusEnum = exports.SimpleTaskExecuteItem = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 简单任务执行明细
 */
class SimpleTaskExecuteItem {
    constructor(options) {
        this.id = options.id;
        this.record_id = options.record_id;
        this.mid = options.mid;
        this.proxy_ip = options.proxy_ip;
        this.cookie = options.cookie;
        this.user_agent = options.user_agent;
        this.status = options.status;
        this.execute_time = options.execute_time;
        this.execute_result = options.execute_result;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.SimpleTaskExecuteItem = SimpleTaskExecuteItem;
var ExecuteItemStatusEnum;
(function (ExecuteItemStatusEnum) {
    ExecuteItemStatusEnum[ExecuteItemStatusEnum["WAIT"] = 0] = "WAIT";
    ExecuteItemStatusEnum[ExecuteItemStatusEnum["SUCCESS"] = 1] = "SUCCESS";
    ExecuteItemStatusEnum[ExecuteItemStatusEnum["FAIL"] = 2] = "FAIL";
})(ExecuteItemStatusEnum = exports.ExecuteItemStatusEnum || (exports.ExecuteItemStatusEnum = {}));
exports.SimpleTaskExecuteItemModel = _1.sequelize.define('simple_task_execute_item', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING,
    },
    record_id: sequelize_1.DataTypes.STRING(64),
    mid: sequelize_1.DataTypes.STRING(64),
    proxy_ip: sequelize_1.DataTypes.STRING(255),
    cookie: sequelize_1.DataTypes.TEXT,
    user_agent: sequelize_1.DataTypes.TEXT,
    status: sequelize_1.DataTypes.INTEGER,
    execute_time: sequelize_1.DataTypes.DATE,
    execute_result: sequelize_1.DataTypes.STRING(255),
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=simpleTaskExecuteItem.js.map