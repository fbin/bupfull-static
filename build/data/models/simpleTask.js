"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SimpleTaskModel = exports.TaskStatusEnum = exports.DurationUnitEnum = exports.TaskTypeEnum = exports.SimpleTask = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 简单任务（点赞、投币、收藏、一键三连、分享、每日任务）
 */
class SimpleTask {
    constructor(options) {
        this.id = options.id;
        this.type = options.type;
        this.bvid = options.bvid;
        this.uid = options.uid;
        this.template_code = options.template_code;
        this.execute_duration = options.execute_duration;
        this.duration_unit = options.duration_unit;
        this.times = options.times;
        this.publish_flag = options.publish_flag;
        this.publish_time = options.publish_time;
        this.publish_by = options.publish_by;
        this.publish_remark = options.publish_remark;
        this.status = options.status;
        this.enable_flag = options.enable_flag;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
        this.del_flag = options.del_flag;
    }
}
exports.SimpleTask = SimpleTask;
var TaskTypeEnum;
(function (TaskTypeEnum) {
    TaskTypeEnum["like"] = "Like";
    TaskTypeEnum["add_coins"] = "AddCoins";
    TaskTypeEnum["favorite"] = "Favorite";
    TaskTypeEnum["three_in_one"] = "ThreeInOne";
    TaskTypeEnum["share"] = "Share";
    TaskTypeEnum["daily"] = "Daily";
    TaskTypeEnum["watch_video"] = "Watches";
})(TaskTypeEnum = exports.TaskTypeEnum || (exports.TaskTypeEnum = {}));
var DurationUnitEnum;
(function (DurationUnitEnum) {
    DurationUnitEnum["H"] = "H";
    DurationUnitEnum["M"] = "M";
    DurationUnitEnum["S"] = "S";
})(DurationUnitEnum = exports.DurationUnitEnum || (exports.DurationUnitEnum = {}));
var TaskStatusEnum;
(function (TaskStatusEnum) {
    TaskStatusEnum[TaskStatusEnum["draft"] = 1] = "draft";
    TaskStatusEnum[TaskStatusEnum["wait_execute"] = 2] = "wait_execute";
    TaskStatusEnum[TaskStatusEnum["on_execute"] = 3] = "on_execute";
    TaskStatusEnum[TaskStatusEnum["execute_complete"] = 4] = "execute_complete";
})(TaskStatusEnum = exports.TaskStatusEnum || (exports.TaskStatusEnum = {}));
exports.SimpleTaskModel = _1.sequelize.define('simple_task', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING(64),
    },
    type: sequelize_1.DataTypes.STRING(32),
    bvid: sequelize_1.DataTypes.STRING(64),
    uid: sequelize_1.DataTypes.STRING(64),
    template_code: sequelize_1.DataTypes.STRING(32),
    execute_duration: sequelize_1.DataTypes.INTEGER,
    duration_unit: sequelize_1.DataTypes.STRING,
    times: sequelize_1.DataTypes.INTEGER,
    publish_flag: sequelize_1.DataTypes.BOOLEAN,
    publish_time: {
        type: sequelize_1.DataTypes.DATE,
        allowNull: true,
    },
    publish_by: sequelize_1.DataTypes.STRING(64),
    publish_remark: sequelize_1.DataTypes.STRING(64),
    status: sequelize_1.DataTypes.INTEGER,
    enable_flag: sequelize_1.DataTypes.BOOLEAN,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
    del_flag: sequelize_1.DataTypes.BOOLEAN,
});
//# sourceMappingURL=simpleTask.js.map