"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SysRoleModel = exports.SysRole = void 0;
const _1 = require(".");
const sequelize_1 = require("sequelize");
/**
 * 系统角色
 */
class SysRole {
    constructor(options) {
        this.id = options.id;
        this.name = options.name;
        this.remark = options.remark;
        this.del_flag = options.del_flag;
        this.create_by = options.create_by;
        this.create_time = options.create_time;
        this.update_by = options.update_by;
        this.update_time = options.update_time;
    }
}
exports.SysRole = SysRole;
exports.SysRoleModel = _1.sequelize.define('sys_role', {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: sequelize_1.DataTypes.INTEGER,
    },
    name: sequelize_1.DataTypes.STRING(32),
    remark: sequelize_1.DataTypes.STRING(255),
    del_flag: sequelize_1.DataTypes.BOOLEAN,
    create_by: sequelize_1.DataTypes.STRING(64),
    create_time: sequelize_1.DataTypes.DATE,
    update_by: sequelize_1.DataTypes.STRING(64),
    update_time: sequelize_1.DataTypes.DATE,
});
//# sourceMappingURL=sysRole.js.map