'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add altering commands here.
         *
         */
        await queryInterface.addColumn('bili_account', 'current_level', {
            type: Sequelize.INTEGER,
            allowNull: true,
            defaultValue: 1, // 可选，字段默认值
        });
        await queryInterface.addColumn('bili_account', 'current_exp', {
            type: Sequelize.INTEGER,
            allowNull: true,
            defaultValue: 200, // 可选，字段默认值
        });
        await queryInterface.addColumn('bili_account', 'next_exp', {
            type: Sequelize.INTEGER,
            allowNull: true,
            defaultValue: 200, // 可选，字段默认值
        });
    },
    async down(queryInterface, Sequelize) {
        /**
         * Add reverting commands here.
         *
         */
        // 如果需要撤销字段新增操作，可以在 down 方法中编写代码
        await queryInterface.removeColumn('bili_account', 'current_level');
        await queryInterface.removeColumn('bili_account', 'current_exp');
        await queryInterface.removeColumn('bili_account', 'next_exp');
    }
};
//# sourceMappingURL=20230824040735-add_level_info_columns_to_bili_account_table.js.map