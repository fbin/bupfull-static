'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.addColumn('bili_account', 'remarks', {
            type: Sequelize.STRING,
            allowNull: true,
            defaultValue: '', // 可选，字段默认值
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.removeColumn('bili_account', 'remarks');
    }
};
//# sourceMappingURL=20230821231815-add_remarks_column_to_bili_account_table.js.map