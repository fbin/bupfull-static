'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.addColumn('bili_account', 'user_agent', {
            type: Sequelize.TEXT,
            allowNull: true,
            defaultValue: '', // 可选，字段默认值
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.removeColumn('bili_account', 'user_agent');
    }
};
//# sourceMappingURL=20230807231312-add_user_agent_column_to_bili_account_table.js.map