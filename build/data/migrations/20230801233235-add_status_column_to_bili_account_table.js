'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        // 在表中新增一个字段
        await queryInterface.addColumn('bili_account', 'status', {
            type: Sequelize.INTEGER,
            allowNull: true,
            defaultValue: 0, // 可选，字段默认值
        });
    },
    async down(queryInterface, Sequelize) {
        // 如果需要撤销字段新增操作，可以在 down 方法中编写代码
        await queryInterface.removeColumn('bili_account', 'status');
    }
};
//# sourceMappingURL=20230801233235-add_status_column_to_bili_account_table.js.map