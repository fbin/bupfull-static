'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.addColumn('comment_task', 'comment_tag_id', {
            type: Sequelize.STRING(64),
            allowNull: false, // 可选，是否允许为空
        });
        await queryInterface.addColumn('comment_task', 'del_flag', {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false, // 可选，字段默认值
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.removeColumn('comment_task', 'comment_tag_id');
        await queryInterface.removeColumn('comment_task', 'del_flag');
    }
};
//# sourceMappingURL=20230918172307-add_tag_and_del_flag_column_to_comment_task_table.js.map