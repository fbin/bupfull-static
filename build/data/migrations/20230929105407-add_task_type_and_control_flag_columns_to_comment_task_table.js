'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.addColumn('comment_task', 'task_type', {
            type: Sequelize.INTEGER,
            allowNull: false, // 可选，是否允许为空
        });
        await queryInterface.addColumn('comment_task', 'control_flag', {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false, // 可选，字段默认值
        });
        await queryInterface.addColumn('comment_task', 'control_time', {
            type: Sequelize.DATE,
            allowNull: true, // 可选，是否允许为空
        });
        await queryInterface.addColumn('comment_task', 'mid_white_list', {
            type: Sequelize.TEXT,
            allowNull: true, // 可选，是否允许为空
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.removeColumn('comment_task', 'task_type');
        await queryInterface.removeColumn('comment_task', 'control_flag');
        await queryInterface.removeColumn('comment_task', 'control_time');
        await queryInterface.removeColumn('comment_task', 'mid_white_list');
    }
};
//# sourceMappingURL=20230929105407-add_task_type_and_control_flag_columns_to_comment_task_table.js.map