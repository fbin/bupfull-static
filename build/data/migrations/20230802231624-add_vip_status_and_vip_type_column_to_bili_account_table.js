'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add altering commands here.
         *
         * Example:
         * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
         */
        await queryInterface.addColumn('bili_account', 'vip_status', {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0, // 可选，字段默认值
        });
        await queryInterface.addColumn('bili_account', 'vip_type', {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0, // 可选，字段默认值
        });
    },
    async down(queryInterface, Sequelize) {
        /**
         * Add reverting commands here.
         *
         * Example:
         * await queryInterface.dropTable('users');
         */
        // 如果需要撤销字段新增操作，可以在 down 方法中编写代码
        await queryInterface.removeColumn('bili_account', 'vip_status');
        await queryInterface.removeColumn('bili_account', 'vip_type');
    }
};
//# sourceMappingURL=20230802231624-add_vip_status_and_vip_type_column_to_bili_account_table.js.map