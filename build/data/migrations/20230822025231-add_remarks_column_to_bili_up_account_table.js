'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.addColumn('bili_up_account', 'remarks', {
            type: Sequelize.STRING,
            allowNull: true,
            defaultValue: '', // 可选，字段默认值
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.removeColumn('bili_up_account', 'remarks');
    }
};
//# sourceMappingURL=20230822025231-add_remarks_column_to_bili_up_account_table.js.map