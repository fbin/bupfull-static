"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const sequelize_1 = require("sequelize");
const cityDistributeTemplate_1 = require("../data/models/cityDistributeTemplate");
const cityDistributeTemplateItem_1 = require("../data/models/cityDistributeTemplateItem");
const sysUser_1 = require("../data/models/sysUser");
let CityTemplateService = class CityTemplateService {
    constructor(logger) {
        this.logger = logger;
    }
    /**
     * 获取城市模板列表
     *
     * @param payloads
     */
    async getList(payloads) {
        const { page_index, page_size, search_text } = payloads;
        const result = await cityDistributeTemplate_1.CityDistributeTemplateModel.findAndCountAll({
            order: [['create_time', 'DESC']],
            offset: (page_index - 1) * page_size,
            limit: page_size
        });
        let list = [];
        for (let i = 0; i < result.rows.length; i++) {
            let _item = result.rows[i].toJSON();
            const itemList = await cityDistributeTemplateItem_1.CityDistributeTemplateItemModel.findAll({
                where: {
                    template_code: _item.template_code,
                },
            });
            const cities = [];
            for (let j = 0; j < itemList.length; j++) {
                cities.push({
                    code: itemList[j].city_code,
                    name: itemList[j].city_name,
                });
            }
            list.push(Object.assign(Object.assign({}, _item), { cities }));
        }
        return {
            code: 0,
            msg: 'Success',
            data: {
                total: result.count,
                list,
            },
        };
    }
    /**
     * 搜索城市模板
     *
     * @param payloads
     */
    async search(payloads) {
        const { search_text } = payloads;
        const where = {};
        if (search_text) {
            where[sequelize_1.Op.or] = [
                { template_code: { [sequelize_1.Op.like]: `%${search_text}%` } },
                { template_name: { [sequelize_1.Op.like]: `%${search_text}%` } }
            ];
        }
        const result = await cityDistributeTemplate_1.CityDistributeTemplateModel.findAll({
            order: [['create_time', 'DESC']],
            where,
        });
        return {
            code: 0,
            msg: 'Success',
            data: result
        };
    }
    ;
    /**
     * 修改城市模板
     *
     * @param payloads 请求参数
     */
    async modify(payloads, accessToken) {
        if (!payloads.cities.length) {
            return { code: 400, message: '模板明细为空' };
        }
        const { template_code, template_name, distribute_type, cities } = payloads;
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken,
            },
        });
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 'system';
        const now = new Date();
        let cityDistributeTemplate = await cityDistributeTemplate_1.CityDistributeTemplateModel.findOne({
            where: {
                template_code: template_code,
            },
        });
        if (cityDistributeTemplate) {
            await cityDistributeTemplate_1.CityDistributeTemplateModel.update({
                template_name: template_name,
                distribute_type: distribute_type,
                update_by: userId,
                update_time: now,
            }, {
                where: {
                    template_code,
                },
            });
            await cityDistributeTemplateItem_1.CityDistributeTemplateItemModel.destroy({
                where: {
                    template_code,
                },
            });
        }
        else {
            await cityDistributeTemplate_1.CityDistributeTemplateModel.create({
                template_code: template_code,
                template_name: template_name,
                distribute_type: distribute_type,
                publish_flag: false,
                publish_by: '',
                publish_time: null,
                publish_remark: '',
                enable_flag: true,
                create_by: userId,
                create_time: now,
                update_by: userId,
                update_time: now,
            });
        }
        // 明细列表
        const items = [];
        cities.forEach((city) => {
            items.push({
                template_code,
                city_code: city.code,
                city_name: city.name,
                enable_flag: true,
                create_by: userId,
                create_time: now,
                update_by: userId,
                update_time: now,
            });
        });
        await cityDistributeTemplateItem_1.CityDistributeTemplateItemModel.bulkCreate(items);
        return { code: 0, msg: '保存成功' };
    }
    /**
     * 删除城市模板
     *
     * @param payloads
     * @param accessToken
     */
    async del(payloads, accessToken) {
        const { template_code } = payloads;
        await cityDistributeTemplate_1.CityDistributeTemplateModel.destroy({
            where: {
                template_code,
            },
        });
        await cityDistributeTemplateItem_1.CityDistributeTemplateItemModel.destroy({
            where: {
                template_code,
            },
        });
        return { code: 0, msg: '删除成功' };
    }
    /**
     * 变更是否启用
     *
     * @param payloads
     * @param accessToken
     * @returns
     */
    async changeEnable(payloads, accessToken) {
        const { template_code, enable_flag } = payloads;
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken,
            },
        });
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 'system';
        const now = new Date();
        let cityDistributeTemplate = await cityDistributeTemplate_1.CityDistributeTemplateModel.findOne({
            where: {
                template_code,
            },
        });
        if (!cityDistributeTemplate) {
            return { code: 400, message: '模板不存在' };
        }
        if (cityDistributeTemplate.enable_flag != enable_flag) {
            await cityDistributeTemplate_1.CityDistributeTemplateModel.update({
                enable_flag,
                update_by: userId,
                update_time: now,
            }, {
                where: {
                    template_code,
                },
            });
        }
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 发布
     *
     * @param payloads
     * @param accessToken
     * @returns
     */
    async publish(payloads, accessToken) {
        const { template_code } = payloads;
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken,
            },
        });
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 'system';
        const now = new Date();
        let cityDistributeTemplate = await cityDistributeTemplate_1.CityDistributeTemplateModel.findOne({
            where: {
                template_code,
            },
        });
        if (!cityDistributeTemplate || !cityDistributeTemplate.enable_flag) {
            return { code: 400, message: '模板不存在或已禁用' };
        }
        if (cityDistributeTemplate.publish_flag) {
            return { code: 400, message: '模板已发布' };
        }
        await cityDistributeTemplate_1.CityDistributeTemplateModel.update({
            publish_flag: true,
            publish_time: now,
            publish_by: userId,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                template_code,
            },
        });
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 获取城市列表
     */
    async getCities() {
        const cities = [{
                value: '010', label: '北京',
            }, {
                value: '020', label: '广州'
            }, {
                value: '021', label: '上海'
            }, {
                value: '025', label: '南京'
            }, {
                value: '0551', label: '合肥'
            }, {
                value: '0571', label: '杭州'
            }, {
                value: '0755', label: '深圳'
            }];
        return { code: 0, msg: 'Success', data: cities };
    }
};
CityTemplateService = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __metadata("design:paramtypes", [winston_1.default.Logger])
], CityTemplateService);
exports.default = CityTemplateService;
//# sourceMappingURL=cityTemplate.js.map