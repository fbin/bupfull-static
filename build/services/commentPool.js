"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const typedi_2 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const sequelize_1 = require("sequelize");
const uuid_1 = require("uuid");
const commentPool_1 = require("../data/models/commentPool");
const tag_1 = require("../data/models/tag");
const sysUser_1 = require("../data/models/sysUser");
const tag_2 = __importDefault(require("../services/tag"));
let CommentPoolService = class CommentPoolService {
    constructor(logger) {
        this.logger = logger;
    }
    /**
     * 获取分页列表
     *
     * @param payloads 请求参数
     */
    async getList(payloads) {
        const { page_index, page_size, tag_id, search_text } = payloads;
        const where = {};
        if (tag_id) {
            Object.assign(where, { tag_id });
        }
        if (search_text) {
            Object.assign(where, {
                comment: {
                    [sequelize_1.Op.like]: `%${search_text}%`,
                },
            });
        }
        const result = await commentPool_1.CommentPoolModel.findAndCountAll({
            where,
            order: [['create_time', 'DESC']],
            offset: (page_index - 1) * page_size,
            limit: page_size,
        });
        let list = [];
        for (let i = 0; i < result.rows.length; i++) {
            let _item = result.rows[i].toJSON();
            const tag = await tag_1.TagModel.findOne({
                where: {
                    id: _item.tag_id,
                },
            });
            list.push(Object.assign(Object.assign({}, _item), { tag }));
        }
        return {
            code: 0,
            msg: 'Success',
            data: {
                total: result.count,
                list,
            },
        };
    }
    /**
     * 新增/修改记录
     *
     * @param payloads 请求参数
     */
    async modify(payloads, accessToken) {
        const { id, comment, tag_id, tag_name } = payloads;
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken,
            },
        });
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 'system';
        const now = new Date();
        let tag;
        if (tag_id) {
            tag = await tag_1.TagModel.findOne({
                where: {
                    id: tag_id,
                    type: tag_1.TagTypeEnum.COMMENT_POOL,
                },
            });
            if (!tag) {
                return { code: 400, msg: '选择的标签不存在' };
            }
        }
        else if (tag_name) {
            tag = await tag_1.TagModel.findOne({
                where: {
                    name: tag_name,
                    type: tag_1.TagTypeEnum.COMMENT_POOL,
                },
            });
            if (!tag) {
                tag = new tag_1.Tag({
                    id: (0, uuid_1.v4)(),
                    name: tag_name,
                    type: tag_1.TagTypeEnum.COMMENT_POOL,
                    enable_flag: true,
                    del_flag: false,
                    create_by: userId,
                    create_time: now,
                    update_by: userId,
                    update_time: now,
                });
                await tag_1.TagModel.create(tag);
            }
        }
        else {
            return { code: 400, msg: '标签为空' };
        }
        let commentPool;
        if (id) {
            commentPool = await commentPool_1.CommentPoolModel.findOne({
                where: {
                    id,
                },
            });
        }
        if (commentPool) {
            await commentPool_1.CommentPoolModel.update({
                comment: comment,
                tag_id: tag.id,
                update_by: userId,
                update_time: now,
            }, {
                where: {
                    id,
                },
            });
        }
        else {
            await commentPool_1.CommentPoolModel.create({
                id,
                comment,
                tag_id: tag.id,
                create_by: userId,
                create_time: now,
                update_by: userId,
                update_time: now,
            });
        }
        return { code: 0, msg: '保存成功' };
    }
    /**
     * 删除记录
     *
     * @param payloads
     * @param accessToken
     */
    async del(payloads, accessToken) {
        const { id } = payloads;
        await commentPool_1.CommentPoolModel.destroy({
            where: {
                id,
            },
        });
        return { code: 0, msg: '删除成功' };
    }
    /**
     * 导入记录
     *
     * @param payloads
     * @param accessToken
     */
    async upload(payloads, accessToken) {
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken,
            },
        });
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 'system';
        const now = new Date();
        const tagService = typedi_2.Container.get(tag_2.default);
        let total = 0;
        for (let i = 0; i < payloads.length; i++) {
            const tag = await tagService.getDb({
                type: tag_1.TagTypeEnum.COMMENT_POOL,
                name: payloads[i].tag_name,
            });
            if (!tag) {
                let tagId = (0, uuid_1.v4)();
                await tagService.create({
                    id: tagId,
                    type: tag_1.TagTypeEnum.COMMENT_POOL,
                    name: payloads[i].tag_name,
                    enable_flag: true,
                    del_flag: false,
                    create_by: userId,
                    create_time: now,
                    update_by: userId,
                    update_time: now,
                });
                await commentPool_1.CommentPoolModel.create({
                    id: (0, uuid_1.v4)(),
                    comment: payloads[i].comment,
                    tag_id: tagId,
                    create_by: userId,
                    create_time: now,
                    update_by: userId,
                    update_time: now,
                });
            }
            else {
                const commentPool = await commentPool_1.CommentPoolModel.findOne({
                    where: {
                        comment: payloads[i].comment,
                        tag_id: tag.id,
                    },
                });
                if (!commentPool) {
                    await commentPool_1.CommentPoolModel.create({
                        id: (0, uuid_1.v4)(),
                        comment: payloads[i].comment,
                        tag_id: tag.id,
                        create_by: userId,
                        create_time: now,
                        update_by: userId,
                        update_time: now,
                    });
                }
            }
            total++;
        }
        return { total };
    }
};
CommentPoolService = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __metadata("design:paramtypes", [winston_1.default.Logger])
], CommentPoolService);
exports.default = CommentPoolService;
//# sourceMappingURL=commentPool.js.map