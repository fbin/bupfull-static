"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const proxyIp_1 = require("../data/models/proxyIp");
const sysUser_1 = require("../data/models/sysUser");
let ProxyIpService = class ProxyIpService {
    constructor(logger) {
        this.logger = logger;
    }
    /**
     * 获取列表
     *
     * @param payloads
     */
    async getList(payloads) {
        const { page_index, page_size, search_text } = payloads;
        const result = await proxyIp_1.ProxyIpModel.findAndCountAll({
            order: [['create_time', "DESC"]],
            offset: (page_index - 1) * page_size,
            limit: page_size,
            where: {
                del_flag: false
            }
        });
        return {
            code: 0,
            msg: 'Success',
            data: {
                total: result.count,
                list: result.rows
            }
        };
    }
    /**
     * 创建代理IP
     *
     * @param payloads 请求参数
     */
    async modify(payloads, accessToken) {
        const { ip, ip_type: ipType, city_code: cityCode, city_name: cityName, isp_code: ispCode, isp_name: ispName, effective_time: effectiveTime, expire_time: expireTime } = payloads;
        let proxyIp = await proxyIp_1.ProxyIpModel.findOne({
            where: {
                ip
            }
        });
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken
            }
        });
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || "system";
        const now = new Date();
        if (proxyIp) {
            await proxyIp_1.ProxyIpModel.update({
                ip_type: ipType,
                city_code: cityCode,
                city_name: cityName,
                isp_code: ispCode,
                isp_name: ispName,
                effective_time: effectiveTime,
                expire_time: expireTime,
                del_flag: false,
                update_by: userId,
                update_time: now
            }, {
                where: {
                    ip
                }
            });
        }
        else {
            await proxyIp_1.ProxyIpModel.create({
                ip,
                ip_type: ipType,
                city_code: cityCode,
                city_name: cityName,
                isp_code: ispCode,
                isp_name: ispName,
                effective_time: effectiveTime,
                expire_time: expireTime,
                enable_flag: true,
                del_flag: false,
                create_by: userId,
                create_time: now,
                update_by: userId,
                update_time: now
            });
        }
        return { code: 0, msg: "操作成功" };
    }
    /**
     * 删除代理IP
     *
     * @param payloads
     * @param accessToken
     * @returns
     */
    async del(payloads, accessToken) {
        const { ip } = payloads;
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken
            }
        });
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || "system";
        const now = new Date();
        // 逻辑删除
        let proxyIp = await proxyIp_1.ProxyIpModel.findOne({
            where: {
                ip
            }
        });
        if (!proxyIp) {
            return { code: 400, msg: 'IP不存在' };
        }
        if (proxyIp.del_flag) {
            return { code: 400, msg: 'IP已删除' };
        }
        await proxyIp_1.ProxyIpModel.update({
            del_flag: true,
            update_by: userId,
            update_time: now
        }, {
            where: {
                ip
            }
        });
        return { code: 0, msg: '删除成功' };
    }
    /**
     * 变更是否启用
     * @param payloads
     * @param accessToken
     * @returns
     */
    async changeEnable(payloads, accessToken) {
        const { ip, enable_flag } = payloads;
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken
            }
        });
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || "system";
        const now = new Date();
        let proxyIp = await proxyIp_1.ProxyIpModel.findOne({
            where: {
                ip
            }
        });
        if (!proxyIp) {
            return { code: 400, message: 'IP不存在' };
        }
        if (proxyIp.del_flag) {
            return { code: 400, message: 'IP已删除' };
        }
        if (proxyIp.enable_flag != enable_flag) {
            await proxyIp_1.ProxyIpModel.update({
                enable_flag,
                update_by: userId,
                update_time: now
            }, {
                where: {
                    ip
                }
            });
        }
        return { code: 0, msg: '操作成功' };
    }
};
ProxyIpService = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __metadata("design:paramtypes", [winston_1.default.Logger])
], ProxyIpService);
exports.default = ProxyIpService;
//# sourceMappingURL=proxyIp.js.map