"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var UserService_1;
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const util_1 = require("../config/util");
const config_1 = __importDefault(require("../config"));
const fs = __importStar(require("fs"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const preset_default_1 = require("@otplib/preset-default");
const auth_1 = require("../data/models/auth");
const notify_1 = __importDefault(require("./notify"));
const schedule_1 = __importDefault(require("./schedule"));
const sock_1 = __importDefault(require("./sock"));
const dayjs_1 = __importDefault(require("dayjs"));
const sysUser_1 = require("../data/models/sysUser");
let UserService = UserService_1 = class UserService {
    constructor(logger, scheduleService, sockService) {
        this.logger = logger;
        this.scheduleService = scheduleService;
        this.sockService = sockService;
    }
    /**
     * 获取系统是否已经初始化
     */
    async getSystemInit() {
        let sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                super_admin: true
            }
        });
        return sysUser != null;
    }
    async login(payloads, req, needTwoFactor = true) {
        // 超级管理员账号是否存在
        let systemInit = await this.getSystemInit();
        if (!systemInit) {
            return { code: 100, message: '系统未初始化' };
        }
        // 取出payloads中的username，password变量
        let { username, password } = payloads;
        // 当前时间
        const timestamp = Date.now();
        // 获取系统用户信息
        let sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                username: payloads.username
            }
        });
        // 判断用户信息
        if (!sysUser) {
            return {
                code: 4001,
                message: '用户名不存在',
            };
        }
        // 读取用户信息
        let { id: userId, username: cUsername, login_pwd: cPassword, latest_login_time: lastlogon, latest_login_ip: lastip } = sysUser;
        // 判断失败次数
        let retries = UserService_1.retryTimesMap.get(username) || 0;
        const retriesTime = Math.pow(3, retries) * 1000;
        if (lastlogon && retries > 2 && timestamp - lastlogon.getTime() < retriesTime) {
            const waitTime = Math.ceil((retriesTime - (timestamp - lastlogon.getTime())) / 1000);
            return {
                code: 410,
                message: `失败次数过多，请${waitTime}秒后重试`,
                data: waitTime,
            };
        }
        // 获取ip和地址信息
        const { ip, address } = await (0, util_1.getNetIp)(req);
        // 账号和密码相等
        if (username === cUsername && password === cPassword) {
            // 生成token
            const data = new String(sysUser.id);
            const expiration = 20;
            let token = jsonwebtoken_1.default.sign({ data }, config_1.default.secret, {
                expiresIn: 60 * 60 * 24 * expiration,
                algorithm: 'HS384',
            });
            // 发送登录通知
            await this.notificationService.notify('登录通知', `你于${(0, dayjs_1.default)(timestamp).format('YYYY-MM-DD HH:mm:ss')}在 ${address} ${req.platform}端 登录成功，ip地址 ${ip}`);
            // 获取登录日志
            await this.getLoginLog();
            // 写入DB，更新鉴权信息
            await this.insertDb({
                user_id: userId,
                ip: ip,
                type: auth_1.AuthDataType.loginLog,
                info: {
                    timestamp,
                    address,
                    ip,
                    platform: req.platform,
                    status: auth_1.LoginStatus.success,
                },
            });
            // 删除重试次数记录
            UserService_1.retryTimesMap.delete(username);
            // 更新用户access_token
            await sysUser_1.SysUserModel.update({
                access_token: token,
                latest_login_ip: ip,
                latest_login_time: new Date(),
                update_by: 'system',
                update_time: new Date(),
            }, {
                where: {
                    id: sysUser.id
                },
                returning: true
            });
            return {
                code: 200,
                data: { token, lastip, lastaddr: address, lastlogon, retries, platform: req.platform },
            };
        }
        else {
            await this.notificationService.notify('登录通知', `你于${(0, dayjs_1.default)(timestamp).format('YYYY-MM-DD HH:mm:ss')}在 ${address} ${req.platform}端 登录失败，ip地址 ${ip}`);
            await this.getLoginLog();
            await this.insertDb({
                user_id: userId,
                ip: ip,
                type: auth_1.AuthDataType.loginLog,
                info: {
                    timestamp,
                    address,
                    ip,
                    platform: req.platform,
                    status: auth_1.LoginStatus.fail,
                },
            });
            UserService_1.retryTimesMap.set(username, retries + 1);
            if (retries > 2) {
                const waitTime = Math.round(Math.pow(3, retries + 1));
                return {
                    code: 410,
                    message: `失败次数过多，请${waitTime}秒后重试`,
                    data: waitTime,
                };
            }
            else {
                return { code: 400, message: config_1.default.authError };
            }
        }
    }
    async logout(platform, access_token) {
        // 获取鉴权信息
        // const authInfo = this.getAuthInfo();
        // 更新鉴权信息
        // this.updateAuthInfo(authInfo, {
        //   token: '',
        //   tokens: { ...authInfo.tokens, [platform]: '' },
        // });
        // 获取用户信息
        let sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: access_token
            }
        });
        // 更新鉴权信息
        if (!sysUser) {
            return {
                code: 401,
                message: '鉴权参数无效，登出失败'
            };
        }
        // 置空access_token
        await sysUser_1.SysUserModel.update({
            access_token: '',
        }, {
            where: {
                id: sysUser.id,
            },
            returning: true
        });
    }
    async getLoginLog() {
        const docs = await auth_1.AuthModel.findAll({
            where: { type: auth_1.AuthDataType.loginLog },
        });
        if (docs && docs.length > 0) {
            const result = docs.sort((a, b) => b.info.timestamp - a.info.timestamp);
            if (result.length > 100) {
                await auth_1.AuthModel.destroy({
                    where: { id: result[result.length - 1].id },
                });
            }
            return result.map((x) => x.info);
        }
        return [];
    }
    async insertDb(payload) {
        const doc = await auth_1.AuthModel.create(Object.assign({}, payload), { returning: true });
        return doc;
    }
    initAuthInfo() {
        fs.writeFileSync(config_1.default.authConfigFile, JSON.stringify({
            init: true
        }));
        return {
            code: 100,
            message: '未找到认证文件，重新初始化',
        };
    }
    async updateUsernameAndPassword({ username, password, }) {
        if (password === 'admin') {
            return { code: 400, message: '密码不能设置为admin' };
        }
        // 判断超级管理员账号是否初始化
        if (await this.getSystemInit()) {
            return { code: 400, message: '超级管理员账号已初始化' };
        }
        const now = new Date();
        // 将超级管理员的初始数据写入数据库
        await sysUser_1.SysUserModel.create({
            username: username,
            login_pwd: password,
            super_admin: true,
            create_by: 'system',
            create_time: now,
            update_by: 'system',
            update_time: now
        }, {
            returning: true
        });
        return { code: 200, message: '更新成功' };
    }
    async updateAvatar(accessToken, avatar) {
        let sysUser = await this.getSysUser(accessToken);
        if (!sysUser) {
            return { code: 400, message: "鉴权参数无效" };
        }
        await sysUser_1.SysUserModel.update({
            avatar: avatar
        }, {
            where: {
                id: sysUser.id
            },
            returning: true
        });
        return { code: 200, data: avatar, message: '更新成功' };
    }
    async getUserInfo(accessToken) {
        // 判断初始化
        let systemInit = await this.getSystemInit();
        if (!systemInit) {
            return { code: 400, message: '系统暂未初始化' };
        }
        // 判断access_token是否有效
        let sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken
            }
        });
        if (!sysUser) {
            return { code: 400, message: '鉴权参数无效' };
        }
        return {
            code: 200, message: 'Success', data: sysUser
        };
    }
    async initTwoFactor(username) {
        let sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                username: username
            }
        });
        if (!sysUser) {
            return { code: 400, message: '用户名不存在' };
        }
        const secret = preset_default_1.authenticator.generateSecret();
        const authInfo = this.getAuthInfo();
        const otpauth = preset_default_1.authenticator.keyuri(username, 'qinglong', secret);
        this.updateAuthInfo(authInfo, { twoFactorSecret: secret });
        return { secret, url: otpauth };
    }
    activeTwoFactor(code) {
        // const authInfo = this.getAuthInfo();
        // const isValid = authenticator.verify({
        //   token: code,
        //   secret: authInfo.twoFactorSecret,
        // });
        // if (isValid) {
        //   this.updateAuthInfo(authInfo, { twoFactorActivated: true });
        // }
        // return isValid;
        return true;
    }
    async twoFactorLogin({ username, password, code, }, req) {
        const authInfo = this.getAuthInfo();
        // const { isTwoFactorChecking, twoFactorSecret } = authInfo;
        // if (!isTwoFactorChecking) {
        //   return { code: 450, message: '未知错误' };
        // }
        // const isValid = authenticator.verify({
        //   token: code,
        //   secret: twoFactorSecret,
        // });
        const isValid = false;
        if (isValid) {
            return this.login({ username, password }, req, false);
        }
        else {
            const { ip, address } = await (0, util_1.getNetIp)(req);
            this.updateAuthInfo(authInfo, {
                lastip: ip,
                lastaddr: address,
                platform: req.platform,
            });
            return { code: 430, message: '验证失败' };
        }
    }
    deactiveTwoFactor() {
        const authInfo = this.getAuthInfo();
        this.updateAuthInfo(authInfo, {
            twoFactorActivated: false,
            twoFactorActived: false,
            twoFactorSecret: '',
        });
        return true;
    }
    async getAuthInfo() {
        const content = fs.readFileSync(config_1.default.authConfigFile, 'utf8');
        return JSON.parse(content || '{}');
    }
    async getSysUser(accessToken) {
        let sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken
            }
        });
        return sysUser;
    }
    updateAuthInfo(authInfo, info) {
        fs.writeFileSync(config_1.default.authConfigFile, JSON.stringify(Object.assign(Object.assign({}, authInfo), info)));
    }
    async getNotificationMode() {
        const doc = await this.getDb({ type: auth_1.AuthDataType.notification });
        return (doc && doc.info) || {};
    }
    async updateAuthDb(payload) {
        let doc = await auth_1.AuthModel.findOne({ type: payload.type });
        if (doc) {
            const updateResult = await auth_1.AuthModel.update(payload, {
                where: { id: doc.id },
                returning: true,
            });
            doc = updateResult[1][0];
        }
        else {
            doc = await auth_1.AuthModel.create(payload, { returning: true });
        }
        return doc;
    }
    async getDb(query) {
        const doc = await auth_1.AuthModel.findOne({ where: Object.assign({}, query) });
        return doc && doc.get({ plain: true });
    }
    async updateNotificationMode(accessToken, notificationInfo) {
        const code = Math.random().toString().slice(-6);
        const isSuccess = await this.notificationService.testNotify(notificationInfo, 'BUP', `【蛟龙】测试通知 https://t.me/jiao_long`);
        if (isSuccess) {
            const sysUser = await this.getSysUser(accessToken);
            const userId = (sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 0;
            const result = await this.updateAuthDb({
                user_id: userId,
                type: auth_1.AuthDataType.notification,
                info: Object.assign({}, notificationInfo),
            });
            return { code: 200, data: Object.assign(Object.assign({}, result), { code }) };
        }
        else {
            return { code: 400, message: '通知发送失败，请检查参数' };
        }
    }
};
// 失败次数map
UserService.retryTimesMap = new Map();
// 默认userId
UserService.defaultUserId = -1;
__decorate([
    (0, typedi_1.Inject)((type) => notify_1.default),
    __metadata("design:type", notify_1.default)
], UserService.prototype, "notificationService", void 0);
UserService = UserService_1 = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __metadata("design:paramtypes", [winston_1.default.Logger, schedule_1.default,
        sock_1.default])
], UserService);
exports.default = UserService;
//# sourceMappingURL=user.js.map