"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const models_1 = require("../data/models");
const sequelize_1 = require("sequelize");
const uuid_1 = require("uuid");
const util_1 = require("../config/util");
const const_1 = require("../config/const");
const commentTask_1 = require("../data/models/commentTask");
const simpleTask_1 = require("../data/models/simpleTask");
const sysUser_1 = require("../data/models/sysUser");
const cityDistributeTemplate_1 = require("../data/models/cityDistributeTemplate");
const cityDistributeTemplateItem_1 = require("../data/models/cityDistributeTemplateItem");
const simpleTaskExecuteRecord_1 = require("../data/models/simpleTaskExecuteRecord");
const simpleTaskExecuteItem_1 = require("../data/models/simpleTaskExecuteItem");
const commentTaskExecuteRecord_1 = require("../data/models/commentTaskExecuteRecord");
const commentTaskExecuteItem_1 = require("../data/models/commentTaskExecuteItem");
const biliAccount_1 = require("../data/models/biliAccount");
const biliUpAccount_1 = require("../data/models/biliUpAccount");
const bili_bot_services_1 = require("../protos/bili_bot_services");
const simpleTaskExecuteStatistic_1 = require("../data/models/simpleTaskExecuteStatistic");
const commentTaskExecuteStatistic_1 = require("../data/models/commentTaskExecuteStatistic");
const typedi_2 = require("typedi");
const cron_1 = __importDefault(require("../services/cron"));
const tag_1 = require("../data/models/tag");
const commentPool_1 = require("../data/models/commentPool");
let BiliTaskService = class BiliTaskService {
    constructor(logger) {
        this.logger = logger;
        this.COMMENT_RUN_TASKS = 'PostComments'; // 评论任务
        this.CONTROL_COMMENT_RUN_TASKS = 'PullAndDelComments'; // 评论删除任务
    }
    /**
     * 获取简单任务列表
     *
     * @param payloads
     * @param accessToken
     */
    async getSimpleList(payloads) {
        const { type, bvid, uid, page_index, page_size } = payloads;
        const where = {};
        if (type != 'ALL') {
            where['type'] = type;
        }
        if (bvid) {
            where['bvid'] = bvid;
        }
        if (uid) {
            where['uid'] = uid;
        }
        const result = await simpleTask_1.SimpleTaskModel.findAndCountAll({
            order: [['create_time', 'DESC']],
            offset: (page_index - 1) * page_size,
            limit: page_size,
            where: Object.assign(Object.assign({}, where), { del_flag: false }),
        });
        let list = [];
        for (let i = 0; i < result.rows.length; i++) {
            let _item = result.rows[i].toJSON();
            // 城市模板信息
            const cityTemplate = await cityDistributeTemplate_1.CityDistributeTemplateModel.findOne({
                where: {
                    template_code: _item.template_code,
                },
            });
            // UP主信息
            const biliUpAccount = await biliUpAccount_1.BiliUpAccountModel.findOne({
                where: {
                    mid: _item.uid,
                },
            });
            list.push(Object.assign(Object.assign({}, _item), { template_name: cityTemplate.template_name, bili_up_account: {
                    mid: _item.uid,
                    name: biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.name,
                    avatar: biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.avatar,
                } }));
        }
        return {
            code: 0,
            msg: 'Success',
            data: {
                total: result.count,
                list,
            },
        };
    }
    /**
     * 编辑简单任务
     *
     * @param payloads 请求参数
     * @param accessToken 访问TOKEN
     */
    async modifySimple(payloads, accessToken) {
        const { id, type, bvid, uid, template_code, execute_duration, duration_unit, times, } = payloads;
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken,
            },
        });
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 'system';
        const now = new Date();
        // 创建每日任务类型的任务时判断是否存在
        if (type == simpleTask_1.TaskTypeEnum.daily) {
            const simpleTask = await simpleTask_1.SimpleTaskModel.findOne({
                where: {
                    type: simpleTask_1.TaskTypeEnum.daily,
                    del_flag: false,
                },
            });
            if (simpleTask) {
                return { code: 1, msg: '已存在相同类型的任务' };
            }
        }
        // 根据ID是否有值判断是新增还是修改
        if (id) {
            const simpleTask = await simpleTask_1.SimpleTaskModel.findOne({
                where: {
                    id,
                },
            });
            // 判断ID是否有效
            if (!simpleTask || simpleTask.del_flag) {
                return { code: 1, msg: 'ID参数无效或已删除' };
            }
            // 已发布不可修改
            if (simpleTask.publish_flag) {
                return { code: 1, msg: '已发布的任务不可修改' };
            }
            await simpleTask_1.SimpleTaskModel.update({
                type,
                bvid,
                uid,
                template_code,
                execute_duration,
                duration_unit,
                times,
                update_by: userId,
                update_time: now,
            }, {
                where: {
                    id: simpleTask.id,
                },
                returning: true,
            });
        }
        else {
            await simpleTask_1.SimpleTaskModel.create({
                id: (0, uuid_1.v4)(),
                type,
                bvid,
                uid,
                template_code,
                execute_duration,
                duration_unit,
                times,
                publish_flag: false,
                status: simpleTask_1.TaskStatusEnum.draft,
                enable_flag: true,
                del_flag: false,
                create_by: userId,
                create_time: now,
                update_by: userId,
                update_time: now,
            });
        }
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 发布简单任务
     *
     * @param payloads 请求参数
     * @param accessToken 访问TOKEN
     */
    async publishSimple(payloads, accessToken) {
        const { id } = payloads;
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken,
            },
        });
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 'system';
        let simpleTask = await simpleTask_1.SimpleTaskModel.findOne({
            where: {
                id: id,
            },
        });
        if (!simpleTask || simpleTask.del_flag) {
            return { code: 400, msg: 'ID无效, 任务没找到' };
        }
        if (simpleTask.publish_flag == true) {
            return { code: 400, msg: '无效操作, 请刷新后重试' };
        }
        const now = new Date();
        await simpleTask_1.SimpleTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.wait_execute,
            publish_flag: true,
            publish_by: userId,
            publish_time: now,
            publish_remark: '',
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: id,
            },
            returning: true,
        });
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 修改简单任务的状态
     *
     * @param payloads
     * @param accessToken
     */
    async changeSimpleEnable(payloads, accessToken) {
        const { id, enable_flag } = payloads;
        const simpleTask = await simpleTask_1.SimpleTaskModel.findOne({
            where: {
                id,
            },
        });
        if (!simpleTask || simpleTask.del_flag) {
            return { code: 1, msg: 'ID参数无效或已删除' };
        }
        if (simpleTask.enable_flag == enable_flag) {
            return { code: 0, msg: '操作成功' };
        }
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken,
            },
        });
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 'system';
        const now = new Date();
        await simpleTask_1.SimpleTaskModel.update({
            enable_flag,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: simpleTask.id,
            },
            returning: true,
        });
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 删除简单任务
     *
     * @param payloads
     * @param accessToken
     */
    async delSimple(payloads, accessToken) {
        const { id } = payloads;
        const simpleTask = await simpleTask_1.SimpleTaskModel.findOne({
            where: {
                id,
            },
        });
        if (!simpleTask) {
            return { code: 1, msg: 'ID参数无效' };
        }
        if (simpleTask.del_flag) {
            return { code: 1, msg: '该任务已删除' };
        }
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken,
            },
        });
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 'system';
        const now = new Date();
        await simpleTask_1.SimpleTaskModel.update({
            del_flag: true,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: simpleTask.id,
            },
            returning: true,
        });
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 执行简单任务
     *
     * @param payloads 请求参数
     * @param accessToken 访问TOKEN
     */
    async executeSimple(payloads, accessToken) {
        const { id } = payloads;
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken,
            },
        });
        // 获取任务实例
        const simpleTask = await simpleTask_1.SimpleTaskModel.findOne({
            where: {
                id: id,
            },
        });
        if (!simpleTask || simpleTask.del_flag || !simpleTask.publish_flag) {
            return {
                code: 1,
                msg: `该任务未找到或未发布, 任务ID: ${id}`,
            };
        }
        if (simpleTask.status == simpleTask_1.TaskStatusEnum.on_execute) {
            return { code: 1, msg: '该任务正在运行中，请勿重复提交运行' };
        }
        // 获取城市模板
        const { template_code } = simpleTask;
        const cityDistributeTemplate = await cityDistributeTemplate_1.CityDistributeTemplateModel.findOne({
            where: {
                template_code: template_code,
                publish_flag: true,
                enable_flag: true,
            },
        });
        if (!cityDistributeTemplate) {
            return {
                code: 1,
                msg: `城市模板未找到或未发布或已停用, 模版编码: ${template_code}`,
            };
        }
        // 获取城市模版明细
        const cityDistributeTemplateItemList = await cityDistributeTemplateItem_1.CityDistributeTemplateItemModel.findAll({
            where: {
                template_code: template_code,
                enable_flag: true,
            },
            raw: true,
        });
        if (!cityDistributeTemplateItemList.length) {
            return {
                code: 1,
                msg: `有效的城市模板明细为空, 模版编码: ${template_code}`,
            };
        }
        // 分配类型
        const { distribute_type } = cityDistributeTemplate;
        // 判断分配类型
        switch (distribute_type) {
            case cityDistributeTemplate_1.DistributeTypeEnum.ROUND_ROBIN:
                // 以轮询的方式执行简单任务
                await this.roundRobinExecuteSimpleTask(sysUser, simpleTask, cityDistributeTemplateItemList);
                return { code: 0, msg: '执行成功' };
        }
    }
    /**
     * 获取简单任务响应
     *
     * @param payloads
     */
    async getBiliSimpleTaskResp(payloads) {
        const { record_id } = payloads;
        const simpleTaskExecuteRecord = await simpleTaskExecuteRecord_1.SimpleTaskExecuteRecordModel.findOne({
            where: {
                id: record_id,
            },
        });
        if (!simpleTaskExecuteRecord) {
            return { code: 1, msg: 'ID参数无效, 未找到记录' };
        }
        const simpleTask = await simpleTask_1.SimpleTaskModel.findOne({
            where: {
                id: simpleTaskExecuteRecord.task_id,
            },
        });
        if (!simpleTask || simpleTask.del_flag || !simpleTask.enable_flag) {
            return { code: 1, msg: '任务不存在或已禁用' };
        }
        if (simpleTask.status != simpleTask_1.TaskStatusEnum.on_execute) {
            return { code: 1, msg: '任务状态非运行中' };
        }
        const blCookies = [];
        const payLoad = {
            type: '1',
            extra: '',
            blCookies,
        };
        const biliTask = {
            uid: simpleTask.uid,
            bvid: simpleTask.bvid,
            recordId: record_id,
            runTasks: simpleTask.type,
            payLoad,
        };
        const itemList = await simpleTaskExecuteItem_1.SimpleTaskExecuteItemModel.findAll({
            where: {
                record_id: record_id,
                status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
            },
        });
        itemList.forEach((item) => {
            blCookies.push({
                snapId: item.id,
                mid: item.mid,
                proxyIp: item.proxy_ip,
                cookie: item.cookie,
                ua: item.user_agent,
                carrier: undefined,
            });
        });
        return biliTask;
    }
    /**
     * 获取评论任务响应
     *
     * @param payloads
     */
    async getBiliCommentTaskResp(payloads) {
        const { record_id, run_tasks } = payloads;
        const executeRecord = await commentTaskExecuteRecord_1.CommentTaskExecuteRecordModel.findOne({
            where: {
                id: record_id,
            },
        });
        if (!executeRecord) {
            return { code: 1, msg: 'ID参数无效, 未找到记录' };
        }
        const commentTask = await commentTask_1.CommentTaskModel.findOne({
            where: {
                id: executeRecord.task_id,
            },
        });
        if (!commentTask || commentTask.del_flag || !commentTask.enable_flag) {
            return { code: 1, msg: '任务不存在或已禁用' };
        }
        if (commentTask.status != simpleTask_1.TaskStatusEnum.on_execute) {
            return { code: 1, msg: '任务状态非运行中' };
        }
        const blCookies = [];
        const payLoad = {
            type: '1',
            extra: '',
            blCookies,
        };
        const biliTask = {
            uid: commentTask.uid,
            bvid: commentTask.bvid,
            recordId: record_id,
            runTasks: run_tasks,
            payLoad,
        };
        const itemList = await commentTaskExecuteItem_1.CommentTaskExecuteItemModel.findAll({
            where: {
                record_id: record_id,
                status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
            },
        });
        // 存在则将评论塞入列表，不存在则新增BlCookie实例
        itemList.forEach((item) => {
            let biliTaskCarrier;
            if (run_tasks == this.COMMENT_RUN_TASKS) {
                const biliTaskComment = {
                    id: '',
                    content: item.comment,
                };
                biliTaskCarrier = {
                    comments: [biliTaskComment],
                    commentController: undefined,
                };
            }
            else if (run_tasks == this.CONTROL_COMMENT_RUN_TASKS) {
                const controlTime = new Date(commentTask.control_time);
                const timestamp = controlTime.getTime() / 1000;
                const midWhiteList = commentTask.mid_white_list
                    .split('\n')
                    .filter((item) => item !== '')
                    .map((item) => {
                    return {
                        mid: item,
                    };
                });
                const commentController = {
                    controlTime: timestamp.toString(),
                    whiteList: midWhiteList,
                };
                biliTaskCarrier = {
                    comments: [],
                    commentController,
                };
            }
            blCookies.push({
                snapId: item.id,
                mid: item.mid,
                proxyIp: item.proxy_ip,
                cookie: item.cookie,
                ua: item.user_agent,
                carrier: biliTaskCarrier,
            });
        });
        return biliTask;
    }
    /**
     * 统计简单任务结果
     *
     * @param payloads
     */
    async countSimpleTaskResult(payloads) {
        const { snapId, mid, recordId, taskStatus } = payloads;
        // 任务执行记录
        const simpleTaskExecuteRecord = await simpleTaskExecuteRecord_1.SimpleTaskExecuteRecordModel.findOne({
            where: {
                id: recordId,
            },
        });
        if (!simpleTaskExecuteRecord) {
            this.logger.error('SimpleTaskExecuteRecord NotFound, id: {}', recordId);
            return;
        }
        // 任务执行明细
        const simpleTaskExecuteItem = await simpleTaskExecuteItem_1.SimpleTaskExecuteItemModel.findOne({
            where: {
                id: snapId,
            },
        });
        if (!simpleTaskExecuteItem) {
            this.logger.error('SimpleTaskExecuteItem NotFound, id: {}', snapId);
            return;
        }
        // 是否成功
        const success = taskStatus == bili_bot_services_1.TaskResult_TaskStatus.Success;
        // 当前时间
        const now = new Date();
        // 更新明细
        await simpleTaskExecuteItem_1.SimpleTaskExecuteItemModel.update({
            status: success
                ? simpleTaskExecuteItem_1.ExecuteItemStatusEnum.SUCCESS
                : simpleTaskExecuteItem_1.ExecuteItemStatusEnum.FAIL,
            update_by: 'system',
            update_time: now,
        }, {
            where: {
                id: snapId,
            },
            returnning: true,
        });
        // 更新记录
        await simpleTaskExecuteRecord_1.SimpleTaskExecuteRecordModel.update({
            total_count: simpleTaskExecuteRecord.total_count + 1,
            success_count: simpleTaskExecuteRecord.success_count + (success ? 1 : 0),
            update_by: 'system',
            update_time: now,
        }, {
            where: {
                id: recordId,
            },
            returnning: true,
        });
        // 更新运行统计数据
        const fmtDate = (0, util_1.dateFormat)(simpleTaskExecuteRecord.create_time, 2);
        const simpleTaskExecuteStatistic = await simpleTaskExecuteStatistic_1.SimpleTaskExecuteStatisticModel.findOne({
            where: {
                mid,
                type: simpleTaskExecuteStatistic_1.StatisticTypeEnum.DAY,
                date: fmtDate,
            },
        });
        if (simpleTaskExecuteStatistic == null) {
            await simpleTaskExecuteStatistic_1.SimpleTaskExecuteStatisticModel.create({
                id: (0, uuid_1.v4)(),
                mid,
                type: simpleTaskExecuteStatistic_1.StatisticTypeEnum.DAY,
                date: fmtDate,
                total_count: 1,
                success_count: success ? 1 : 0,
                create_by: 'system',
                create_time: now,
                update_by: 'system',
                update_time: now,
            });
        }
        else {
            await simpleTaskExecuteStatistic_1.SimpleTaskExecuteStatisticModel.update({
                total_count: simpleTaskExecuteStatistic.total_count + 1,
                success_count: simpleTaskExecuteStatistic.success_count + (success ? 1 : 0),
                update_by: 'system',
                update_time: now,
            }, {
                where: {
                    id: simpleTaskExecuteStatistic.id,
                },
                returnning: true,
            });
        }
    }
    /**
     * 统计评论任务结果
     *
     * @param payloads
     */
    async countCommentTaskResult(payloads) {
        const { snapId, mid, recordId, taskStatus } = payloads;
        // 任务执行记录
        const executeRecord = await commentTaskExecuteRecord_1.CommentTaskExecuteRecordModel.findOne({
            where: {
                id: recordId,
            },
        });
        if (!executeRecord) {
            this.logger.error('CommentTaskExecuteRecord NotFound, id: {}', recordId);
            return;
        }
        // 任务执行明细
        const executeItem = await commentTaskExecuteItem_1.CommentTaskExecuteItemModel.findOne({
            where: {
                id: snapId,
            },
        });
        if (!executeItem) {
            this.logger.error('CommentTaskExecuteItem NotFound, id: {}', snapId);
            return;
        }
        // 是否成功
        const success = taskStatus == bili_bot_services_1.TaskResult_TaskStatus.Success;
        // 当前时间
        const now = new Date();
        // 更新明细
        await commentTaskExecuteItem_1.CommentTaskExecuteItemModel.update({
            status: success
                ? simpleTaskExecuteItem_1.ExecuteItemStatusEnum.SUCCESS
                : simpleTaskExecuteItem_1.ExecuteItemStatusEnum.FAIL,
            update_by: 'system',
            update_time: now,
        }, {
            where: {
                id: snapId,
            },
            returnning: true,
        });
        // 更新记录
        await commentTaskExecuteRecord_1.CommentTaskExecuteRecordModel.update({
            total_count: executeRecord.total_count + 1,
            success_count: executeRecord.success_count + (success ? 1 : 0),
            update_by: 'system',
            update_time: now,
        }, {
            where: {
                id: recordId,
            },
            returnning: true,
        });
        // 更新运行统计数据
        const fmtDate = (0, util_1.dateFormat)(executeRecord.create_time, 2);
        const executeStatistic = await commentTaskExecuteStatistic_1.CommentTaskExecuteStatisticModel.findOne({
            where: {
                mid,
                type: simpleTaskExecuteStatistic_1.StatisticTypeEnum.DAY,
                date: fmtDate,
            },
        });
        if (executeStatistic == null) {
            await commentTaskExecuteStatistic_1.CommentTaskExecuteStatisticModel.create({
                id: (0, uuid_1.v4)(),
                mid,
                type: simpleTaskExecuteStatistic_1.StatisticTypeEnum.DAY,
                date: fmtDate,
                total_count: 1,
                success_count: success ? 1 : 0,
                create_by: 'system',
                create_time: now,
                update_by: 'system',
                update_time: now,
            });
        }
        else {
            await simpleTaskExecuteStatistic_1.SimpleTaskExecuteStatisticModel.update({
                total_count: executeStatistic.total_count + 1,
                success_count: executeStatistic.success_count + (success ? 1 : 0),
                update_by: 'system',
                update_time: now,
            }, {
                where: {
                    id: executeStatistic.id,
                },
                returnning: true,
            });
        }
    }
    /**
     * 结束执行简单任务
     *
     * @param payloads
     */
    async endSimpleTask(payloads) {
        const { recordId } = payloads;
        // 任务执行记录
        const simpleTaskExecuteRecord = await simpleTaskExecuteRecord_1.SimpleTaskExecuteRecordModel.findOne({
            where: {
                id: recordId,
            },
        });
        if (!simpleTaskExecuteRecord) {
            this.logger.error('SimpleTaskExecuteRecord NotFound, id: {}', recordId);
            return;
        }
        // 任务
        const simpleTask = await simpleTask_1.SimpleTaskModel.findOne({
            where: {
                id: simpleTaskExecuteRecord.task_id,
            },
        });
        if (!simpleTask) {
            this.logger.error('SimpleTask NotFound, id: {}', recordId);
            return;
        }
        // 当前时间
        const now = new Date();
        await simpleTask_1.SimpleTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.execute_complete,
            update_by: 'system',
            update_time: now,
        }, {
            where: {
                id: simpleTask.id,
            },
            returnning: true,
        });
    }
    /**
     * 结束执行评论任务
     *
     * @param payloads
     */
    async endCommentTask(payloads) {
        const { recordId } = payloads;
        // 任务执行记录
        const executeRecord = await commentTaskExecuteRecord_1.CommentTaskExecuteRecordModel.findOne({
            where: {
                id: recordId,
            },
        });
        if (!executeRecord) {
            this.logger.error('CommentTaskExecuteRecord NotFound, id: {}', recordId);
            return;
        }
        // 任务
        const commentTask = await commentTask_1.CommentTaskModel.findOne({
            where: {
                id: executeRecord.task_id,
            },
        });
        if (!commentTask) {
            this.logger.error('CommentTask NotFound, id: {}', recordId);
            return;
        }
        // 当前时间
        const now = new Date();
        await commentTask_1.CommentTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.execute_complete,
            update_by: 'system',
            update_time: now,
        }, {
            where: {
                id: commentTask.id,
            },
            returnning: true,
        });
    }
    /**
     * 以轮询的方式执行简单任务
     *
     * @param sysUser
     * @param simpleTask
     * @param cityDistributeTemplateItemList
     */
    async roundRobinExecuteSimpleTask(sysUser, simpleTask, cityDistributeTemplateItemList) {
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 'system';
        const now = new Date();
        // 创建任务执行记录
        let recordId = (0, uuid_1.v4)();
        await simpleTaskExecuteRecord_1.SimpleTaskExecuteRecordModel.create({
            id: recordId,
            schedule_type: simpleTaskExecuteRecord_1.ScheduleTypeEnum.RIGHT_NOW,
            schedule_id: recordId,
            task_id: simpleTask.id,
            start_time: now,
            total_count: simpleTask.times,
            success_count: 0,
            create_by: userId,
            create_time: now,
            update_by: userId,
            update_time: now,
        });
        // 启动BiliBot
        switch (simpleTask.type) {
            case simpleTask_1.TaskTypeEnum.daily:
                // 每日任务类型特殊处理
                this.createDailyTaskItems(userId, now, recordId, simpleTask, cityDistributeTemplateItemList);
                break;
            case simpleTask_1.TaskTypeEnum.like:
            case simpleTask_1.TaskTypeEnum.add_coins:
            case simpleTask_1.TaskTypeEnum.favorite:
            case simpleTask_1.TaskTypeEnum.three_in_one:
            case simpleTask_1.TaskTypeEnum.share:
            case simpleTask_1.TaskTypeEnum.watch_video:
            default:
                // 非每日任务类型按分配逻辑处理
                this.createOtherTaskItems(userId, now, recordId, simpleTask, cityDistributeTemplateItemList);
                break;
        }
        // 将任务状态设置为运行中
        await simpleTask_1.SimpleTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.on_execute,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: simpleTask.id,
            },
            returnning: true,
        });
        // 执行定时任务
        const cronService = typedi_2.Container.get(cron_1.default);
        // 启动BiliBot
        switch (simpleTask.type) {
            case simpleTask_1.TaskTypeEnum.like:
                cronService.runTask(const_1.CRON_NAME_DIC['1'], simpleTask.id, recordId);
                break;
            case simpleTask_1.TaskTypeEnum.add_coins:
                cronService.runTask(const_1.CRON_NAME_DIC['2'], simpleTask.id, recordId);
                break;
            case simpleTask_1.TaskTypeEnum.favorite:
                cronService.runTask(const_1.CRON_NAME_DIC['3'], simpleTask.id, recordId);
                break;
            case simpleTask_1.TaskTypeEnum.three_in_one:
                cronService.runTask(const_1.CRON_NAME_DIC['4'], simpleTask.id, recordId);
                break;
            case simpleTask_1.TaskTypeEnum.share:
                cronService.runTask(const_1.CRON_NAME_DIC['5'], simpleTask.id, recordId);
                break;
            case simpleTask_1.TaskTypeEnum.daily:
                cronService.runTask(const_1.CRON_NAME_DIC['6'], simpleTask.id, recordId);
                break;
            case simpleTask_1.TaskTypeEnum.watch_video:
                cronService.runTask(const_1.CRON_NAME_DIC['7'], simpleTask.id, recordId);
                break;
        }
    }
    /**
     * 创建非每日任务类型任务明细
     *
     * @param userId
     * @param now
     * @param recordId
     * @param simpleTask
     * @param cityDistributeTemplateItemList
     */
    async createDailyTaskItems(userId, now, recordId, simpleTask, cityDistributeTemplateItemList) {
        const durationSecs = this.getDurationSecs(simpleTask.execute_duration, simpleTask.duration_unit);
        // 每次毫秒数
        const perTimesMilliSecs = (durationSecs * 1000) / simpleTask.times;
        // 根据标签获取需要升级的小号列表
        const itemLen = cityDistributeTemplateItemList.length;
        for (let i = 0; i < itemLen; i++) {
            const cityDistributeTemplateItem = cityDistributeTemplateItemList[i];
            let cityCode = cityDistributeTemplateItem.city_code;
            // 获取查询结果
            const queryResult = await models_1.sequelize.query('select t.* ' +
                'from (' +
                'select t1.mid, t1.login_time, t1.proxy_ip, t1.cookie, t1.user_agent ' +
                'from bili_account t1 ' +
                'join proxy_ip t2 on t1.proxy_ip = t2.ip and t2.enable_flag = 1 and t2.city_code = ? ' +
                'join bili_account_tag t3 on t1.mid = t3.mid ' +
                'join tag t4 on t3.tag_id = t4.id and t4.type = ? and t4.name = ? ' +
                'where t1.login_status = 1 and t1.current_level < 6) t ', {
                type: sequelize_1.QueryTypes.SELECT,
                replacements: [cityCode, tag_1.TagTypeEnum.BILI_ACCOUNT, '每日练号'],
                logging: false,
            });
            if (queryResult.length > 0) {
                // 任务明细列表
                let simpleTaskExecuteItemList = [];
                // 解析查询结果
                queryResult.forEach((biliAccount, index) => {
                    simpleTaskExecuteItemList.push({
                        id: (0, uuid_1.v4)(),
                        record_id: recordId,
                        mid: biliAccount.mid,
                        proxy_ip: biliAccount.proxy_ip || '',
                        cookie: biliAccount.cookie,
                        user_agent: biliAccount.user_agent || '',
                        status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
                        execute_time: new Date(now.getTime() + index * perTimesMilliSecs),
                        execute_result: '',
                        create_by: userId,
                        create_time: now,
                        update_by: userId,
                        update_time: now,
                    });
                });
                // 写入数据库
                if (simpleTaskExecuteItemList.length > 0) {
                    await simpleTaskExecuteItem_1.SimpleTaskExecuteItemModel.bulkCreate(simpleTaskExecuteItemList, {
                        returnning: true,
                    });
                }
            }
        }
    }
    /**
     * 创建非每日任务类型任务明细
     *
     * @param userId
     * @param now
     * @param recordId
     * @param simpleTask
     * @param cityDistributeTemplateItemList
     */
    async createOtherTaskItems(userId, now, recordId, simpleTask, cityDistributeTemplateItemList) {
        const durationSecs = this.getDurationSecs(simpleTask.execute_duration, simpleTask.duration_unit);
        // 每次毫秒数
        const perTimesMilliSecs = (durationSecs * 1000) / simpleTask.times;
        const itemLen = cityDistributeTemplateItemList.length;
        // 平均每个城市分配的次数
        let avgPerCityTimes = (simpleTask.times / itemLen) | 0;
        let index = 0;
        while (index < itemLen) {
            let limit = avgPerCityTimes;
            if (index == itemLen - 1) {
                limit = simpleTask.times - avgPerCityTimes * index;
            }
            let cityDistributeTemplateItem = cityDistributeTemplateItemList[index];
            // 根据城市编码获取执行任务最少的小号
            let cityCode = cityDistributeTemplateItem.city_code;
            // 获取查询结果
            const queryResult = await models_1.sequelize.query('select t.* ' +
                'from (' +
                'select t1.mid, t1.login_time, t1.proxy_ip, t1.cookie, t1.user_agent, ' +
                'ifnull(t3.total_count, 0) as total_count ' +
                'from bili_account t1 ' +
                'join proxy_ip t2 on t1.proxy_ip = t2.ip and t2.enable_flag = 1 and t2.city_code = ? ' +
                'left join simple_task_execute_statistic t3 on t1.mid = t3.mid and t3.type = 3 and t3.date = ? ' +
                'where t1.login_status = 1 ) t ' +
                'order by t.total_count asc, t.login_time asc ' +
                'limit ?', {
                type: sequelize_1.QueryTypes.SELECT,
                replacements: [cityCode, (0, util_1.dateFormat)(now, 2), limit],
                logging: false,
            });
            if (queryResult.length > 0) {
                // 任务明细列表
                let simpleTaskExecuteItemList = [];
                // 解析查询结果
                queryResult.forEach((biliAccount, index) => {
                    simpleTaskExecuteItemList.push({
                        id: (0, uuid_1.v4)(),
                        record_id: recordId,
                        mid: biliAccount.mid,
                        proxy_ip: biliAccount.proxy_ip || '',
                        cookie: biliAccount.cookie,
                        user_agent: biliAccount.user_agent || '',
                        status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
                        execute_time: new Date(now.getTime() + index * perTimesMilliSecs),
                        execute_result: '',
                        create_by: userId,
                        create_time: now,
                        update_by: userId,
                        update_time: now,
                    });
                });
                // 写入数据库
                if (simpleTaskExecuteItemList.length > 0) {
                    await simpleTaskExecuteItem_1.SimpleTaskExecuteItemModel.bulkCreate(simpleTaskExecuteItemList, {
                        returnning: true,
                    });
                }
            }
            index++;
        }
    }
    /**
     * 获取时长，单位为秒
     *
     * @param executeDuration
     * @param durationUnit
     * @returns
     */
    getDurationSecs(executeDuration, durationUnit) {
        switch (String(durationUnit)) {
            case simpleTask_1.DurationUnitEnum.H:
                return executeDuration * 60 * 60;
            case simpleTask_1.DurationUnitEnum.M:
                return executeDuration * 60;
            case simpleTask_1.DurationUnitEnum.S:
                return executeDuration;
            default:
                throw new Error('时长单位不支持');
        }
    }
    /**
     * 获取评论任务列表
     *
     * @param payloads
     * @param accessToken
     */
    async getCommentList(payloads) {
        const { page_index, page_size } = payloads;
        const where = {};
        const result = await commentTask_1.CommentTaskModel.findAndCountAll({
            order: [['create_time', 'DESC']],
            offset: (page_index - 1) * page_size,
            limit: page_size,
            where: Object.assign(Object.assign({}, where), { del_flag: false }),
        });
        let list = [];
        for (let i = 0; i < result.rows.length; i++) {
            let _item = result.rows[i].toJSON();
            // 城市模板信息
            const cityTemplate = await cityDistributeTemplate_1.CityDistributeTemplateModel.findOne({
                where: {
                    template_code: _item.template_code,
                },
            });
            // UP主信息
            const biliUpAccount = await biliUpAccount_1.BiliUpAccountModel.findOne({
                where: {
                    mid: _item.uid,
                },
            });
            // 标签信息
            const commentTag = await tag_1.TagModel.findOne({
                where: {
                    id: _item.comment_tag_id,
                },
            });
            list.push(Object.assign(Object.assign({}, _item), { template_name: cityTemplate === null || cityTemplate === void 0 ? void 0 : cityTemplate.template_name, bili_up_account: {
                    mid: _item.uid,
                    name: biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.name,
                    avatar: biliUpAccount === null || biliUpAccount === void 0 ? void 0 : biliUpAccount.avatar,
                }, comment_tag: commentTag }));
        }
        return {
            code: 0,
            msg: 'Success',
            data: {
                total: result.count,
                list,
            },
        };
    }
    /**
     * 创建评论任务
     *
     * @param payloads 请求参数
     * @param accessToken 访问TOKEN
     */
    async modifyComment(payloads, accessToken) {
        const { id, task_type, uid, bvid, template_code, comment_tag_id, control_time, mid_white_list, } = payloads;
        // 校验参数
        if (commentTask_1.CommentTaskTypeEnum.PUBLISH == task_type) {
            if (!template_code) {
                return { code: -1, msg: '城市模板为空' };
            }
        }
        else {
            if (!control_time) {
                return { code: -1, msg: '请设置控评时间' };
            }
            if (!mid_white_list) {
                return { code: -1, msg: '评论白名单为空' };
            }
        }
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken,
            },
        });
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 'system';
        const now = new Date();
        if (id) {
            let commentTask = await commentTask_1.CommentTaskModel.findOne({
                where: {
                    id,
                },
            });
            if (!commentTask || !commentTask.enable_flag || commentTask.del_flag) {
                return { code: 400, msg: '任务已停用或已删除' };
            }
            switch (commentTask.task_type) {
                case commentTask_1.CommentTaskTypeEnum.PUBLISH:
                    await commentTask_1.CommentTaskModel.update({
                        comment_tag_id,
                        template_code,
                        update_by: userId,
                        update_time: now,
                    }, {
                        where: {
                            id: commentTask.id,
                        },
                    });
                    break;
                case commentTask_1.CommentTaskTypeEnum.CONTROL:
                    await commentTask_1.CommentTaskModel.update({
                        control_time,
                        mid_white_list,
                        update_by: userId,
                        update_time: now,
                    }, {
                        where: {
                            id: commentTask.id,
                        },
                    });
                    break;
            }
        }
        else {
            switch (task_type) {
                case commentTask_1.CommentTaskTypeEnum.PUBLISH:
                    await commentTask_1.CommentTaskModel.create({
                        id: (0, uuid_1.v4)(),
                        task_type,
                        uid,
                        bvid,
                        template_code,
                        comment_tag_id,
                        execute_duration: 1,
                        duration_unit: simpleTask_1.DurationUnitEnum.H,
                        comment_type: commentTask_1.CommentTypeEnum.BY_SORT,
                        comment_times: 0,
                        publish_flag: false,
                        status: simpleTask_1.TaskStatusEnum.draft,
                        enable_flag: true,
                        control_flag: false,
                        mid_white_list: '',
                        create_by: userId,
                        create_time: now,
                        update_by: userId,
                        update_time: now,
                        del_flag: false,
                    });
                    break;
                case commentTask_1.CommentTaskTypeEnum.CONTROL:
                    const commentTask = await commentTask_1.CommentTaskModel.findOne({
                        where: {
                            task_type: commentTask_1.CommentTaskTypeEnum.CONTROL,
                            bvid,
                            del_flag: false,
                        },
                    });
                    if (commentTask) {
                        return {
                            code: -1,
                            msg: `任务已存在, 不可重复创建, 视频ID: ${bvid}`,
                        };
                    }
                    await commentTask_1.CommentTaskModel.create({
                        id: (0, uuid_1.v4)(),
                        task_type,
                        uid,
                        bvid,
                        template_code: '',
                        comment_tag_id: '',
                        execute_duration: 1,
                        duration_unit: simpleTask_1.DurationUnitEnum.H,
                        comment_type: commentTask_1.CommentTypeEnum.BY_SORT,
                        comment_times: 0,
                        publish_flag: false,
                        status: simpleTask_1.TaskStatusEnum.draft,
                        enable_flag: true,
                        control_flag: false,
                        control_time,
                        mid_white_list,
                        create_by: userId,
                        create_time: now,
                        update_by: userId,
                        update_time: now,
                        del_flag: false,
                    });
                    break;
            }
        }
        return { code: 0, msg: '保存成功' };
    }
    /**
     * 发布评论任务
     *
     * @param payloads 请求参数
     * @param accessToken 访问TOKEN
     */
    async publishComment(payloads, accessToken) {
        const { id } = payloads;
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken,
            },
        });
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 'system';
        let commentTask = await commentTask_1.CommentTaskModel.findOne({
            where: {
                id: id,
            },
        });
        if (!commentTask) {
            return { code: 400, msg: 'ID无效, 任务没找到' };
        }
        if (commentTask.publish_flag) {
            return { code: 400, msg: '无效操作, 请刷新后重试' };
        }
        const now = new Date();
        await commentTask_1.CommentTaskModel.update({
            publish_flag: true,
            publish_by: userId,
            publish_time: now,
            publish_remark: '',
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: id,
            },
            returning: true,
        });
        return { code: 0, msg: '发布成功' };
    }
    /**
     * 修改评论任务的状态
     *
     * @param payloads
     * @param accessToken
     */
    async changeCommentEnable(payloads, accessToken) {
        const { id, enable_flag } = payloads;
        const commentTask = await commentTask_1.CommentTaskModel.findOne({
            where: {
                id,
            },
        });
        if (!commentTask || commentTask.del_flag) {
            return { code: 1, msg: 'ID参数无效或已删除' };
        }
        if (commentTask.enable_flag == enable_flag) {
            return { code: 0, msg: '操作成功' };
        }
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken,
            },
        });
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 'system';
        const now = new Date();
        await commentTask_1.CommentTaskModel.update({
            enable_flag,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: commentTask.id,
            },
            returning: true,
        });
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 修改评论任务的评论控制开关状态
     *
     * @param payloads
     * @param accessToken
     */
    async changeCommentControl(payloads, accessToken) {
        const { id, control_flag } = payloads;
        const commentTask = await commentTask_1.CommentTaskModel.findOne({
            where: {
                id,
            },
        });
        if (!commentTask || commentTask.del_flag) {
            return { code: 1, msg: 'ID参数无效或已删除' };
        }
        if (commentTask.control_flag == control_flag) {
            return { code: 0, msg: '操作成功' };
        }
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken,
            },
        });
        // 控制开关打开, 判断当前up账号是否已登录
        if (control_flag) {
            const biliUpAccount = await biliAccount_1.BiliAccountModel.findOne({
                where: {
                    mid: commentTask.uid,
                },
            });
            if (!biliUpAccount || !biliUpAccount.cookie) {
                return { code: -1, msg: '请先扫码登录UP主账号' };
            }
        }
        else {
            //开关关闭, 关闭定时任务
            const cronService = typedi_2.Container.get(cron_1.default);
            cronService.delCronByTaskId(commentTask.id);
        }
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 'system';
        const now = new Date();
        await commentTask_1.CommentTaskModel.update({
            control_flag,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: commentTask.id,
            },
            returning: true,
        });
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 删除评论任务
     *
     * @param payloads
     * @param accessToken
     */
    async delComment(payloads, accessToken) {
        const { id } = payloads;
        const commentTask = await commentTask_1.CommentTaskModel.findOne({
            where: {
                id,
            },
        });
        if (!commentTask) {
            return { code: 1, msg: 'ID参数无效' };
        }
        if (commentTask.del_flag) {
            return { code: 1, msg: '该任务已删除' };
        }
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken,
            },
        });
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 'system';
        const now = new Date();
        await commentTask_1.CommentTaskModel.update({
            del_flag: true,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: commentTask.id,
            },
            returning: true,
        });
        return { code: 0, msg: '操作成功' };
    }
    /**
     * 执行评论任务
     *
     * @param payloads 请求参数
     * @param accessToken 访问TOKEN
     */
    async executeComment(payloads, accessToken) {
        const { id } = payloads;
        const sysUser = await sysUser_1.SysUserModel.findOne({
            where: {
                access_token: accessToken,
            },
        });
        // 获取任务实例
        const commentTask = await commentTask_1.CommentTaskModel.findOne({
            where: {
                id: id,
                publish_flag: true,
            },
        });
        if (!commentTask) {
            return {
                code: 400,
                msg: `该任务未找到, 不存在或未发布, 任务ID: ${id}`,
            };
        }
        // 判断一下任务类型
        if (commentTask.task_type == commentTask_1.CommentTaskTypeEnum.CONTROL) {
            // 开启了控制开关
            if (commentTask.control_flag) {
                await this.createAndRunControlCommentTask(sysUser, commentTask);
                return { code: 0, msg: '执行成功' };
            }
            else {
                return { code: -1, msg: '请先开启控评开关' };
            }
        }
        // 获取评论池记录列表
        const commentPoolList = await commentPool_1.CommentPoolModel.findAll({
            where: {
                tag_id: commentTask.comment_tag_id,
            },
            raw: true,
        });
        if (!commentPoolList.length) {
            return {
                code: 400,
                msg: `该标签对应的评论记录为空, 请检查后重新执行`,
            };
        }
        // 获取城市模板
        const { template_code } = commentTask;
        const cityDistributeTemplate = await cityDistributeTemplate_1.CityDistributeTemplateModel.findOne({
            where: {
                template_code: template_code,
                publish_flag: true,
                enable_flag: true,
            },
        });
        if (!cityDistributeTemplate) {
            return {
                code: 400,
                msg: `城市模板未找到, 不存在或未发布或已停用, 模版编码: ${template_code}`,
            };
        }
        // 获取城市模版明细
        const cityDistributeTemplateItemList = await cityDistributeTemplateItem_1.CityDistributeTemplateItemModel.findAll({
            where: {
                template_code: template_code,
                enable_flag: true,
            },
            raw: true,
        });
        if (!cityDistributeTemplateItemList.length) {
            return {
                code: 400,
                msg: `有效的城市模板明细为空, 模版编码: ${template_code}`,
            };
        }
        // 分配类型
        const { distribute_type } = cityDistributeTemplate;
        // 判断分配类型
        switch (distribute_type) {
            case cityDistributeTemplate_1.DistributeTypeEnum.ROUND_ROBIN:
                // 以轮询的方式执行评论任务
                await this.roundRobinExecuteCommentTask(sysUser, commentTask, commentPoolList, cityDistributeTemplateItemList);
                return { code: 0, msg: '执行成功' };
            default:
                return {
                    code: 400,
                    msg: `无效的分配方式, 模版编码: ${template_code}`,
                };
        }
    }
    async createAndRunControlCommentTask(sysUser, commentTask) {
        const biliUpAccount = await biliAccount_1.BiliAccountModel.findOne({
            where: {
                mid: commentTask.uid,
            },
        });
        // 创建任务执行记录
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 'system';
        const now = new Date();
        let recordId = (0, uuid_1.v4)();
        await commentTaskExecuteRecord_1.CommentTaskExecuteRecordModel.create({
            id: recordId,
            schedule_type: simpleTaskExecuteRecord_1.ScheduleTypeEnum.RIGHT_NOW,
            schedule_id: recordId,
            task_id: commentTask.id,
            start_time: now,
            total_count: 1,
            success_count: 0,
            create_by: userId,
            create_time: now,
            update_by: userId,
            update_time: now,
        });
        // 评控任务明细列表
        let controlTaskExecuteItem = {
            id: (0, uuid_1.v4)(),
            record_id: recordId,
            mid: biliUpAccount.mid,
            comment: '',
            proxy_ip: biliUpAccount.proxy_ip || '',
            cookie: biliUpAccount.cookie,
            user_agent: biliUpAccount.user_agent || '',
            status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
            execute_time: new Date(now.getTime()),
            execute_result: '',
            create_by: userId,
            create_time: now,
            update_by: userId,
            update_time: now,
        };
        // 写入数据库
        await commentTaskExecuteItem_1.CommentTaskExecuteItemModel.create(controlTaskExecuteItem, {
            returnning: true,
        });
        // 将任务状态设置为运行中
        await commentTask_1.CommentTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.on_execute,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: commentTask.id,
            },
            returnning: true,
        });
        // 执行定时任务
        const cronService = typedi_2.Container.get(cron_1.default);
        // 启动BiliBot
        cronService.runTask(const_1.CRON_NAME_DIC['9'], commentTask.id, recordId);
    }
    /**
     * 以轮询的方式执行评论任务
     *
     * @param sysUser
     * @param commentTask
     * @param commentPoolList
     * @param cityDistributeTemplateItemList
     */
    async roundRobinExecuteCommentTask(sysUser, commentTask, commentPoolList, cityDistributeTemplateItemList) {
        const userId = String(sysUser === null || sysUser === void 0 ? void 0 : sysUser.id) || 'system';
        const now = new Date();
        const durationSecs = this.getDurationSecs(commentTask.execute_duration, commentTask.duration_unit);
        let commentCount = commentPoolList.length;
        // 每次毫秒数
        const perTimesMilliSecs = (durationSecs * 1000) / commentCount;
        // 创建任务执行记录
        let recordId = (0, uuid_1.v4)();
        await commentTaskExecuteRecord_1.CommentTaskExecuteRecordModel.create({
            id: recordId,
            schedule_type: simpleTaskExecuteRecord_1.ScheduleTypeEnum.RIGHT_NOW,
            schedule_id: recordId,
            task_id: commentTask.id,
            start_time: now,
            total_count: commentCount,
            success_count: 0,
            create_by: userId,
            create_time: now,
            update_by: userId,
            update_time: now,
        });
        // 平均每个城市分配多少条
        let avgPerCityCount = (commentCount / cityDistributeTemplateItemList.length) | 0;
        let index = 0;
        let commentIndex = 0;
        while (index < cityDistributeTemplateItemList.length) {
            // 平均每个城市要发送的评论数
            let limit = avgPerCityCount;
            if (index == cityDistributeTemplateItemList.length - 1) {
                limit = commentCount - avgPerCityCount * index;
            }
            // 城市模板明细
            let cityDistributeTemplateItem = cityDistributeTemplateItemList[index];
            // 根据城市编码获取执行任务最少的小号
            let cityCode = cityDistributeTemplateItem.city_code;
            // 获取待执行发送评论的小号列表
            const results = await models_1.sequelize.query('select t.* ' +
                'from (' +
                'select t1.mid, t1.login_time, t1.proxy_ip, t1.cookie, t1.user_agent, ' +
                'ifnull(t3.total_count, 0) as total_count ' +
                'from bili_account t1 ' +
                'join proxy_ip t2 on t1.proxy_ip = t2.ip and t2.enable_flag = 1 and t2.city_code = :cityCode ' +
                'left join comment_task_execute_statistic t3 on t1.mid = t3.mid and t3.type = 3 and t3.date = :date ' +
                'where t1.login_status = 1 ) t ' +
                'order by t.total_count asc, t.login_time asc ' +
                'limit :limit', {
                type: sequelize_1.QueryTypes.SELECT,
                replacements: {
                    cityCode,
                    date: (0, util_1.dateFormat)(now, 2),
                    limit,
                },
            });
            // 任务明细列表
            let taskExecuteItemList = [];
            // 遍历要发送的评论数, 小号不够时, 需要再从头取小号
            for (let i = 0; i < limit; i++) {
                // 取小号
                let biliAccount = results[i % results.length];
                // 取评论
                let comment = commentPoolList[commentIndex++].comment;
                // 记录数据
                taskExecuteItemList.push({
                    id: (0, uuid_1.v4)(),
                    record_id: recordId,
                    mid: biliAccount.mid,
                    comment: comment,
                    proxy_ip: biliAccount.proxy_ip || '',
                    cookie: biliAccount.cookie,
                    user_agent: biliAccount.user_agent || '',
                    status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
                    execute_time: new Date(now.getTime() + index * perTimesMilliSecs),
                    execute_result: '',
                    create_by: userId,
                    create_time: now,
                    update_by: userId,
                    update_time: now,
                });
            }
            // 写入数据库
            if (taskExecuteItemList.length > 0) {
                await commentTaskExecuteItem_1.CommentTaskExecuteItemModel.bulkCreate(taskExecuteItemList, {
                    returnning: true,
                });
            }
            index++;
        }
        // 将任务状态设置为运行中
        await commentTask_1.CommentTaskModel.update({
            status: simpleTask_1.TaskStatusEnum.on_execute,
            update_by: userId,
            update_time: now,
        }, {
            where: {
                id: commentTask.id,
            },
            returnning: true,
        });
        // 执行定时任务
        const cronService = typedi_2.Container.get(cron_1.default);
        // 启动BiliBot
        cronService.runTask(const_1.CRON_NAME_DIC['8'], commentTask.id, recordId);
    }
};
BiliTaskService = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __metadata("design:paramtypes", [winston_1.default.Logger])
], BiliTaskService);
exports.default = BiliTaskService;
//# sourceMappingURL=biliTask.js.map