"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const winston_1 = __importDefault(require("winston"));
const models_1 = require("../data/models");
const sequelize_1 = require("sequelize");
const biliAccount_1 = require("../data/models/biliAccount");
const tag_1 = require("../data/models/tag");
let BiliAccountService = class BiliAccountService {
    constructor(logger) {
        this.logger = logger;
    }
    async create(payload) {
        const bUser = new biliAccount_1.BiliAccount(payload);
        const doc = await this.insert(bUser);
        return doc;
    }
    async insert(payload) {
        return await biliAccount_1.BiliAccountModel.create(payload, { returning: true });
    }
    async update(payload) {
        const doc = await this.getDb({ mid: payload.mid });
        const tab = new biliAccount_1.BiliAccount(Object.assign(Object.assign({}, doc), payload));
        const newDoc = await this.updateDb(tab);
        return newDoc;
    }
    async updateDb(payload) {
        await biliAccount_1.BiliAccountModel.update(payload, { where: { mid: payload.mid } });
        return await this.getDb({ mid: payload.mid });
    }
    async remove(ids) {
        await biliAccount_1.BiliAccountModel.destroy({ where: { mid: ids } });
    }
    async list() {
        try {
            const result = await biliAccount_1.BiliAccountModel.findAll({
                where: {},
                order: [['mid', 'DESC']],
            });
            return result;
        }
        catch (error) {
            throw error;
        }
    }
    async getDb(query) {
        const doc = await biliAccount_1.BiliAccountModel.findOne({ where: Object.assign({}, query) });
        return doc && doc.get({ plain: true });
    }
    async biliAccounts(searchText = '', query = {}) {
        let condition = Object.assign({}, query);
        if (searchText) {
            const encodeText = encodeURI(searchText);
            const reg = {
                [sequelize_1.Op.or]: [
                    { [sequelize_1.Op.like]: `%${searchText}%` },
                    { [sequelize_1.Op.like]: `%${encodeText}%` },
                ],
            };
            condition = Object.assign(Object.assign({}, condition), { [sequelize_1.Op.or]: [
                    {
                        name: reg,
                    },
                ] });
        }
        try {
            const biliAccounts = await this.find(condition, [
                ['create_time', 'ASC'],
            ]);
            const resultList = [];
            for (let i = 0; i < biliAccounts.length; i++) {
                let biliAccount = biliAccounts[i];
                let sql = 'select t2.id, t2.type, t2.name ' +
                    'from bili_account_tag t1 ' +
                    'join tag t2 on t1.tag_id = t2.id and t2.type = ? ' +
                    'where t1.mid = ? ' +
                    'order by t1.create_time asc';
                // 获取查询结果
                const queryTags = await models_1.sequelize.query(sql, {
                    type: sequelize_1.QueryTypes.SELECT,
                    replacements: [tag_1.TagTypeEnum.BILI_ACCOUNT, biliAccount.mid],
                    logging: false,
                });
                const _item = Object.assign(Object.assign({}, biliAccount), { tags: queryTags });
                resultList.push(_item);
            }
            return resultList;
        }
        catch (error) {
            console.error('BiliAccountService envs error', error);
            throw error;
        }
    }
    async getTrainingList(searchText = '', query = {}) {
        let sql = 'select t1.* ' +
            'from bili_account t1 ' +
            'join bili_account_tag t2 on t1.mid = t2.mid ' +
            'join tag t3 on t2.tag_id = t3.id and t3.type = ? and t3.name = ? ' +
            'where t1.current_level < 6 ';
        if (searchText) {
            sql += `and t1.name = ${searchText} `;
        }
        sql += 'order by t1.current_exp desc';
        // 获取查询结果
        const queryResult = await models_1.sequelize.query(sql, {
            type: sequelize_1.QueryTypes.SELECT,
            replacements: [tag_1.TagTypeEnum.BILI_ACCOUNT, '每日练号'],
            logging: false,
        });
        return { code: 200, data: queryResult };
    }
    async find(query, sort = []) {
        const docs = await biliAccount_1.BiliAccountModel.findAll({
            where: Object.assign({}, query),
            order: [...sort],
            raw: true,
        });
        return docs;
    }
    async disabled(ids) {
        await biliAccount_1.BiliAccountModel.update({ status: biliAccount_1.AccountStatus.disabled }, { where: { mid: ids } });
    }
    async enabled(ids) {
        await biliAccount_1.BiliAccountModel.update({ status: biliAccount_1.AccountStatus.normal }, { where: { mid: ids } });
    }
};
BiliAccountService = __decorate([
    (0, typedi_1.Service)(),
    __param(0, (0, typedi_1.Inject)('logger')),
    __metadata("design:paramtypes", [winston_1.default.Logger])
], BiliAccountService);
exports.default = BiliAccountService;
//# sourceMappingURL=biliAccount.js.map