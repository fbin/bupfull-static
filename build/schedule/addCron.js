"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.addCron = void 0;
const node_schedule_1 = __importDefault(require("node-schedule"));
const data_1 = require("./data");
const runCron_1 = require("../shared/runCron");
const typedi_1 = require("typedi");
const simpleTask_1 = require("../data/models/simpleTask");
const simpleTaskExecuteRecord_1 = require("../data/models/simpleTaskExecuteRecord");
const simpleTaskExecuteItem_1 = require("../data/models/simpleTaskExecuteItem");
const commentTask_1 = require("../data/models/commentTask");
const commentTaskExecuteRecord_1 = require("../data/models/commentTaskExecuteRecord");
const commentTaskExecuteItem_1 = require("../data/models/commentTaskExecuteItem");
const const_1 = require("../config/const");
const addCron = (call, callback) => {
    var _a;
    const logger = typedi_1.Container.get('logger');
    for (const item of call.request.crons) {
        const { id, recordId, schedule, command, cName } = item;
        if (data_1.scheduleStacks.has(id)) {
            (_a = data_1.scheduleStacks.get(id)) === null || _a === void 0 ? void 0 : _a.cancel();
        }
        const cmdStr = `ID=${id} RecordId=${recordId} ${command}`;
        logger.info('Add schedule cron task: ' + cmdStr);
        data_1.scheduleStacks.set(id, node_schedule_1.default.scheduleJob(id, schedule, async () => {
            if (cName === const_1.CRON_NAME_DIC['9']) {
                const commentTaskExecuteRecord = await commentTaskExecuteRecord_1.CommentTaskExecuteRecordModel.findOne({
                    where: {
                        id: recordId,
                    },
                });
                if (commentTaskExecuteRecord) {
                    const commentTask = await commentTask_1.CommentTaskModel.findOne({
                        where: {
                            id: commentTaskExecuteRecord.task_id,
                        },
                    });
                    if (commentTask && commentTask.enable_flag && !commentTask.del_flag) {
                        // 将任务状态设置为运行中
                        await commentTask_1.CommentTaskModel.update({
                            status: simpleTask_1.TaskStatusEnum.on_execute,
                            update_time: new Date(),
                        }, {
                            where: {
                                id: commentTask.id,
                            },
                            returnning: true,
                        });
                        await commentTaskExecuteItem_1.CommentTaskExecuteItemModel.update({
                            status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
                            update_time: new Date(),
                        }, {
                            where: {
                                record_id: recordId,
                            },
                            returnning: true,
                        });
                        (0, runCron_1.runCron)(cmdStr);
                    }
                }
                return;
            }
            const simpleTaskExecuteRecord = await simpleTaskExecuteRecord_1.SimpleTaskExecuteRecordModel.findOne({
                where: {
                    id: recordId,
                },
            });
            if (simpleTaskExecuteRecord) {
                const simpleTask = await simpleTask_1.SimpleTaskModel.findOne({
                    where: {
                        id: simpleTaskExecuteRecord.task_id,
                    },
                });
                if (simpleTask && simpleTask.enable_flag && !simpleTask.del_flag) {
                    // 将任务状态设置为运行中
                    await simpleTask_1.SimpleTaskModel.update({
                        status: simpleTask_1.TaskStatusEnum.on_execute,
                        update_time: new Date(),
                    }, {
                        where: {
                            id: simpleTask.id,
                        },
                        returnning: true,
                    });
                    await simpleTaskExecuteItem_1.SimpleTaskExecuteItemModel.update({
                        status: simpleTaskExecuteItem_1.ExecuteItemStatusEnum.WAIT,
                        update_time: new Date(),
                    }, {
                        where: {
                            record_id: recordId,
                        },
                        returnning: true,
                    });
                    (0, runCron_1.runCron)(cmdStr);
                }
            }
        }));
    }
    callback(null, null);
};
exports.addCron = addCron;
//# sourceMappingURL=addCron.js.map