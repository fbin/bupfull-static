"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.delCron = void 0;
const data_1 = require("./data");
const typedi_1 = require("typedi");
const delCron = (call, callback) => {
    var _a;
    const logger = typedi_1.Container.get('logger');
    for (const id of call.request.ids) {
        if (data_1.scheduleStacks.has(id)) {
            (_a = data_1.scheduleStacks.get(id)) === null || _a === void 0 ? void 0 : _a.cancel();
            data_1.scheduleStacks.delete(id);
            logger.info('Delete schedule cron task: ' + id);
        }
    }
    callback(null, null);
};
exports.delCron = delCron;
//# sourceMappingURL=delCron.js.map